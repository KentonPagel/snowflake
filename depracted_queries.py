#!/usr/bin/python3
"""
Snowflake Queries!
"

import os
import sys
import pprint
import argparse
import traceback
from datetime import datetime, timedelta
import snowflake.connector as snow_conn
import pandas as pd
import json
import matplotlib.pyplot as plt
from pyvin import VIN
from vininfo import Vin
from time import sleep
import csv
import sys

from config import config

def filter_vehicle_faults_response(data_frame_object=None):
    """
    Retrieve debug info from nested JSON object.
    :param data_frame_object:
    :return:
    """
    list_of_data = list()
    if data_frame_object is not None:
        for index, row in enumerate(data_frame_object.itertuples()):
            list_to_append = list()
            if VEHICLE_FAULTS_DISPLAY['Asset ID'] is True:
                list_to_append.append(str(row.ASSET_ID))
            if VEHICLE_FAULTS_DISPLAY['CM'] is True:
                list_to_append.append(str(row.CM))
            if VEHICLE_FAULTS_DISPLAY['Created At'] is True:
                list_to_append.append(str(row.CREATED_AT))
            if VEHICLE_FAULTS_DISPLAY['Dbt ID'] is True:
                list_to_append.append(str(row.DBT_ID))
            if VEHICLE_FAULTS_DISPLAY['Deleted At'] is True:
                list_to_append.append(str(row.DELETED_AT))
            if VEHICLE_FAULTS_DISPLAY['Deployment ID'] is True:
                list_to_append.append(str(row.DEPLOYMENT_ID))
            if VEHICLE_FAULTS_DISPLAY['Device ID'] is True:
                list_to_append.append(str(row.DEVICE_ID))
            if VEHICLE_FAULTS_DISPLAY['Fault ID'] is True:
                list_to_append.append(str(row.FAULT_ID))
            if VEHICLE_FAULTS_DISPLAY['FMI'] is True:
                list_to_append.append(str(row.FMI))
            if VEHICLE_FAULTS_DISPLAY['ID'] is True:
                list_to_append.append(str(row.ID))
            if VEHICLE_FAULTS_DISPLAY['Meta'] is True:
                list_to_append.append(str(row.META))
            if VEHICLE_FAULTS_DISPLAY['OC'] is True:
                list_to_append.append(str(row.OC))
            if VEHICLE_FAULTS_DISPLAY['SA'] is True:
                list_to_append.append(str(row.SA))
            if VEHICLE_FAULTS_DISPLAY['SPN'] is True:
                list_to_append.append(str(row.SPN))
            if VEHICLE_FAULTS_DISPLAY['Started At'] is True:
                list_to_append.append(str(row.STARTED_AT))
            if VEHICLE_FAULTS_DISPLAY['Stopped At'] is True:
                list_to_append.append(str(row.STOPPED_AT))
            if VEHICLE_FAULTS_DISPLAY['Updated At'] is True:
                list_to_append.append(str(row.UPDATED_AT))
            if VEHICLE_FAULTS_DISPLAY['User ID'] is True:
                list_to_append.append(str(row.USER_ID))
            if VEHICLE_FAULTS_DISPLAY['UUID'] is True:
                list_to_append.append(str(row.UUID))
            if VEHICLE_FAULTS_DISPLAY['Veh Obs Id'] is True:
                list_to_append.append(str(row.VEHICLE_OBSERVATIONS_ID))

            list_of_data.append(list_to_append)
            for index, element in enumerate(list_of_data):
                for index_inner, variable in enumerate(element):
                    # Remove redundant parentheses
                    if variable is not None:
                        list_of_data[index][index_inner] = variable.replace("'", "")
                    else:
                        list_of_data[index][index_inner] = "?"

        return list_of_data
    else:
        print("WARNING! Tried to print debug info!")

def fault_data():
    """
    Get vehicle fault data
    """
    def print_table(rows):
        max_widths = []
        for column in zip(*rows):
            max_widths.append(max([len(text) for text in column]))
        template = '  '.join(['{{:<{}}}'.format(width) for width in max_widths])
        return '\n'.join([template.format(*row) for row in rows])

    list_of_heartbeated_esns = []

    sql_cmd = '\nSELECT \n'
    sql_cmd += '* \n'
    sql_cmd += ' from "PROD"."ORION"."VEHICLE_FAULTS" \n'
    if START_DATE and END_DATE:
        sql_cmd += 'WHERE created_at BETWEEN \'{}\' and \'{}\' \n'.format(
                START_DATE, END_DATE)
    if FILTER_BY_ENVIRONMENT != '':
        if START_DATE and END_DATE:
            sql_cmd += 'AND \n'
        else:
            sql_cmd += 'WHERE \n'
        sql_cmd += 'utils.deployment_id_to_customer_name(deployment_id) '\
                   '= \'{}\' \n'.format(FILTER_BY_ENVIRONMENT)
    sql_cmd += 'LIMIT {};'.format(LIMIT)
    rsp = execute_query(connection=snowflake_conn,
                        query=sql_cmd,
                        display_response=False)
    # Sort by most recent timestamp
    rsp["CREATED_AT"] = pd.to_datetime(rsp["CREATED_AT"])
    rsp = rsp.sort_values(by="CREATED_AT", ascending=False)

    parsed_results = filter_vehicle_faults_response(data_frame_object=rsp)
    title_row = list()
    filtered_list_to_display = list()
    displayed_esn_list = list()
    for element in VEHICLE_FAULTS_DISPLAY:
        if VEHICLE_FAULTS_DISPLAY[element] is True:
            title_row.append(element)
    parsed_results.insert(0, title_row)
    filtered_list_to_display.append(title_row)
    for row in parsed_results[1:]:
        current_asset_id = row[0]
        list_of_heartbeated_esns.append(current_asset_id)
        if current_asset_id not in displayed_esn_list:
            displayed_esn_list.append(current_asset_id)
            filtered_list_to_display.append(row)

    print("{}".format(print_table(filtered_list_to_display)))
    print("\n# of faults observed = {}".format(len(filtered_list_to_display) - 1))


def sni_update_progress_sql_query(esn_to_search="", desired_upm=""):
    """
    Grab and format data.
    """
    example_sql_cmd = \
        'WITH hb as (\n\
            SELECT\n\
                doc:entities:power_unit:vin::VARCHAR as power_unit_vin,\n\
                doc:entities:cvd:esn as esn,\n\
                date(created_at) as date_hour,\n\
                doc:device_info:cvd_sw:engine_events_ipk:version AS ee_version,\n\
                doc:device_info:cvd_sw:jpod_firmware:version as jpod_firmware,\n\
                doc:device_info:cvd_sw:jpod_config:version as jpod_config,\n\
                doc:device_info:cvd_sw:upm_version:version as upm_version,\n\
                doc:debug:config_version as config_version,\n\
                doc:debug:shutdown_reason as shutdown_reason,\n\
                doc:debug:sensor_failures.external_power_supply.cause \n\
                    as external_power_supply_failure\n\
            FROM orion.telematics_heartbeat \n'
    if FILTER_BY_ENVIRONMENT != '':
        example_sql_cmd += 'WHERE utils.deployment_id_to_customer_name(deployment_id) \n\
                            = \'{}\'\n'.format(FILTER_BY_ENVIRONMENT)
        example_sql_cmd += 'AND \n'
    else:
        example_sql_cmd += 'WHERE \n'
    example_sql_cmd += 'doc:entities.cvd:esn in (\'{}\')\n'.format(esn_to_search)

    example_sql_cmd += 'AND \n'
    example_sql_cmd += 'created_at between \'{}\' and \'{}\'\n\
            GROUP BY esn, date_hour, ee_version, config_version, power_unit_vin, \n\
                     shutdown_reason, external_power_supply_failure, jpod_firmware,  \n\
                     upm_version, jpod_config \n\
        ), hb_vin_decode as (\n\
            SELECT \n\
                hb.esn as esn,\n\
                hb.date_hour as date_hour, \n\
                hb.jpod_firmware as jpod_firmware, \n\
                hb.jpod_config as jpod_config, \n\
                hb.upm_version as upm_version, \n\
                hb.ee_version as ee_version,\n\
                hb.config_version as config_version,\n\
                hb.shutdown_reason as shutdown_reason, \n\
                hb.external_power_supply_failure as external_power_supply_failure \n\
            FROM hb\n\
            LEFT JOIN user_pwatts.vin_decode v on hb.power_unit_vin = v.doc:VIN\n\
        )\n\
        SELECT * FROM hb_vin_decode LIMIT {};'.format(
            START_DATE, END_DATE, LIMIT)
    response = execute_query(connection=snowflake_conn, query=example_sql_cmd,
                             pandas=True, display_request=True, display_response=False)
    # Sort by date before printing
    response["DATE_HOUR"] = pd.to_datetime(response["DATE_HOUR"])
    response = response.sort_values(by="DATE_HOUR", ascending=False)
    print("{}".format(response))
    item_good = False
    esn = esn_to_search
    upm_version = "?"
    ee = "?"
    jpod_cfg = "?"
    jpod_fw = "?"
    try:
        esn = response['ESN'].values[0].replace('"', '')
        upm_version = response['UPM_VERSION'].values[0].replace('"', '')
        ee = response['EE_VERSION'].values[0].replace('"', '')
        jpod_cfg = response['JPOD_CONFIG'].values[0].replace('"', '')
        jpod_fw = response['JPOD_FIRMWARE'].values[0].replace('"', '')
    except IndexError:
        print("Index Error!")

    if upm_version == desired_upm:
        item_good = True

    return item_good, esn, upm_version, ee, jpod_cfg, jpod_fw


def sni_update_progress():
    """
    Diagnose SNI issues.
    """
    DESIRED_UPM_VERSION = ""
    LIST_OF_ESNS = ['']
    CATEGORY = "THREE"

    if CATEGORY == "ONE":
        DESIRED_UPM_VERSION = "3.22.16-277"
        LIST_OF_ESNS = ['5571008480',
                        '5571050986',
                        '5571052433',
                        '5571052810',
                        '5571053438',
                        '5571062727',
                        '5571070947',
                        '5571080630',
                        '5571080952',
                        '5571089225',
                        '5571079033',
                        '5571062614']
    elif CATEGORY == "TWO":
        DESIRED_UPM_VERSION = "3.22.15-276"
        LIST_OF_ESNS = ['5571008740',
                        '5571061519',
                        '5571061919',
                        '5571078040',
                        '5571081464',
                        '5571083285',
                        '5571087285',
                        '5571087647']
    elif CATEGORY == "THREE":
        DESIRED_UPM_VERSION = "3.122.47-30"
        LIST_OF_ESNS = ['5571053828']
    LIST_OF_RESPONSE = []
    for esn in LIST_OF_ESNS:
        print("SNI search: {}..".format(esn))
        item_good, item_esn, item_upm_version,\
            item_ee, item_jpod_config, item_jpod_fw = \
            sni_update_progress_sql_query(esn_to_search=esn, desired_upm=DESIRED_UPM_VERSION)
        response_to_save = "{}  {}  {}  {}           {}      {}".format(
            item_esn, item_upm_version, item_ee, item_jpod_config, item_jpod_fw, item_good)
        LIST_OF_RESPONSE.append(response_to_save)
    print("Looking for {}".format(DESIRED_UPM_VERSION))
    print("Summary:         UPM     EE          JPOD CONFIG  JPOD FW  Updated?")
    for item in LIST_OF_RESPONSE:
        print("{}".format(item))

def vhcl_1802_sni():
    """
    Query for updating SNI live impact. 
    """
    sql_cmd = '\n\
        SELECT \n\
            utils.deployment_id_to_customer_name(hos.deployment_id) as env,\n\
            hos.power_unit_number,\n\
            users.external_id as driver_id,\n\
            device.serial as esn,\n\
            hos.malfunction_status,\n\
            hos.event_timestamp_utc,\n\
            hos.odometer_elapsed,\n\
            hos.duration_seconds\n\
        FROM "PROD"."ORION"."HOS_EVENT_LOG" hos\n\
        JOIN "PROD"."ORION"."USERS" users ON users.id = hos.user_id\n\
        JOIN "PROD"."ORION"."DEVICES"device ON device.id = hos.cvd_id\n\
        WHERE hos.CREATED_AT > current_timestamp - INTERVAL \'11 hour\'\n\
        AND hos.RECORD_ORIGIN=2\n\
        AND hos.EVENT_TYPE=1\n\
        AND hos.EVENT_CODE=3\n\
        AND hos.duration_seconds IS NULL;'
    execute_query(connection=snowflake_conn,
                  query=sql_cmd,
                  pandas=True,
                  display_request=True,
                  display_response=True)


def analyze_critical_events(machine_name=None, start_date=None, end_date=None):
    """
    Critical events: ID, DEPLOYMENT_ID, EVENT_ID, TRIP_ID, HEARTBEAT_ID,
                     ASSET_ID, USER_ID, VEHICLE_OBSERVATIONS_ID, TYPE,
                     MACHINE_NAME, ESN, POWER_UNIT_ID, ABS_STATUS,
                     PARKING_BRAKE, TRIGGER_EVENT

    @:machine_name - emergency_braking_active,
                     follow_time_violation, roll_stability, excessive_over_speed
                     etc TODO
    @:start_date - start date
    @:end_date - end date
    """
    sql_cmd = 'SELECT * from "PROD"."ORION"."VEHICLE_EVENTS" \n'
    if machine_name:
        sql_cmd += 'WHERE machine_name = \'{}\' \n'.format(machine_name)
    if start_date and end_date:
        if not machine_name:
            sql_cmd += 'WHERE \n'
        else:
            sql_cmd += 'AND \n'
        sql_cmd += 'created_at BETWEEN \'{}\' and \'{}\' \n'.format(
            start_date, end_date)
    sql_cmd += 'LIMIT {};\n'.format(LIMIT)
    rsp = execute_query(connection=snowflake_conn, query=sql_cmd)
    plot_data(panda_dataframe=rsp,
              data_dict={'kind': 'line',
                         'x-axis': 'ESN',
                         'y-axis': ['ASSET_ID']},
              show_plot=True,
              title_name='

def analyze_roll_stability():
    """
    Query that Subbu wanted as an example. Looking at roll stability. The graph
    separates lines based on EE version
    """
    # 1. get count of emergency braking on SNI. daily between dates
    # 2. get count of critical events per ESN for each month.
    # 3. any follow up outliers
    # 4. consider being able to grab the config version

    # total count
    # join on telematics heartbeat ESN/date to get version info
    roll_stability_sql_cmd = 'with crit_evt as (\n\
        SELECT\n\
            date(created_at) as date_hour,\n\
            esn\n\
        FROM\n\
            orion.vehicle_events \n\
        WHERE\n\
            1=1\n\
            AND utils.deployment_id_to_customer_name(deployment_id) = \'SNI\'\n\
            AND type = \'critical_event\'\n\
            AND MACHINE_NAME = \'roll_stability\'\n\
            AND created_at between \'2021-05-08\' and \'2021-07-08\'\n\
    ), hb as (\n\
        SELECT\n\
            doc:entities:cvd:esn as esn,\n\
            doc:device_info:cvd_sw:engine_events_ipk:version AS ee_version,\n\
            doc:debug:config_version as config_version,\n\
            date(created_at) as date_hour\n\
        FROM orion.telematics_heartbeat \n\
        WHERE \n\
            utils.deployment_id_to_customer_name(deployment_id) = \'SNI\'\n\
            AND created_at between \'2021-05-08\' and \'2021-07-08\'\n\
        GROUP BY date_hour, esn, ee_version, config_version\n\
    ), counts as (\n\
        SELECT\n\
            crit_evt.date_hour,\n\
            crit_evt.esn,\n\
            hb.ee_version,\n\
            hb.config_version,\n\
            count(*) as total\n\
        FROM crit_evt LEFT JOIN hb\n\
        ON crit_evt.esn = hb.esn AND crit_evt.date_hour = hb.date_hour\n\
        WHERE \n\
            hb.config_version LIKE \'cvd-default-cvd-v2%\'\n\
        GROUP BY crit_evt.date_hour, crit_evt.esn, hb.ee_version, hb.config_version\n\
        ORDER BY crit_evt.date_hour ASC\n\
    )\n\
    SELECT * FROM counts\n\
    WHERE total <= 20\n\
    LIMIT {};'.format(LIMIT)
    execute_query(connection=snowflake_conn, query=roll_stability_sql_cmd)


def analyze_emergency_brake():
    """
    Example to get data for emergency braking events.
    """
    emergency_braking_sql_cmd = '\n\
        WITH historical_device_config_details AS (\n\
          SELECT\n\
              cdcd.deployment_id,\n\
              cdcd.cvd_id,\n\
              cd.my_date as date,\n\
              cdcd.config,\n\
              dcdf.id,\n\
              dcdf.emergency_braking,\n\
              dcdf.hard_brake,\n\
              dcdf.over_speed\n\
          FROM \n\
              prod.device_history.cvd_to_device_config_details cdcd\n\
          INNER JOIN \n\
              prod.utils.calendar_date cd ON (\n\
                cd.my_date between cdcd.start_time and COALESCE(cdcd.end_time, convert_timezone(\'UTC\', current_timestamp))\n\
              )\n\
          INNER JOIN prod.config_history.device_config_details_flattened dcdf ON (\n\
                  cdcd.deployment_id = dcdf.deployment_id\n\
                  AND cd.my_date between dcdf.valid_from AND COALESCE(dcdf.valid_to, convert_timezone(\'UTC\', current_timestamp))\n\
                  AND cdcd.device_config_details_id = dcdf.id\n\
              )\n\
          WHERE\n\
              1 = 1\n\
              AND cd.my_date >= dateadd(days, -30, convert_timezone(\'UTC\', current_timestamp))\n\
        )\n\
        , count_assets AS (\n\
          SELECT\n\
              date,\n\
              deployment_id,\n\
              CASE \n\
                WHEN emergency_braking = 1 THEN cvd_id \n\
                     ELSE null \n\
              END AS emergency_brake_cvd,\n\
              CASE \n\
                WHEN hard_brake = 1 THEN cvd_id \n\
                ELSE NULL\n\
              END AS hard_brake_cvd,\n\
              CASE \n\
                WHEN over_speed = 1 THEN cvd_id \n\
                ELSE NULL \n\
              END AS over_speed_cvd\n\
            FROM \n\
                historical_device_config_details\n\
        )\n\
        , enabled_cnt AS (\n\
              SELECT\n\
                  date AS date_day,\n\
                  prod.utils.deployment_id_to_customer_name(deployment_id) AS environment,\n\
                  count(distinct emergency_brake_cvd)                      AS emergency_brake_cvd_count,\n\
                  count(distinct hard_brake_cvd)                           AS hard_brake_cvd_count,\n\
                  count(distinct over_speed_cvd)                           AS over_speed_cvd_count\n\
              FROM \n\
                count_assets\n\
              GROUP BY date_day, environment\n\
        )\n\
        , baseline AS (\n\
          SELECT\n\
             prod.utils.deployment_id_to_customer_name(v.deployment_id) AS environment,\n\
             v.asset_id,\n\
             date_trunc(hour, logged_at)                                AS date_hour,\n\
             date(v.logged_at)                                          AS date_day,\n\
             date_trunc(month, logged_at)                               AS date_month,\n\
             CASE \n\
                WHEN DAYOFWEEK(logged_at) in (0,6) THEN 0 \n\
                ELSE 1 \n\
             END AS weekday,\n\
             CASE \n\
                WHEN machine_name = \'hard_brake\' THEN 1 ELSE 0 \n\
             END AS hard_brake_event,\n\
             CASE \n\
               WHEN machine_name = \'emergency_braking_active\' THEN 1 ELSE 0 \n\
             END AS emergency_braking_active_event,\n\
             CASE \n\
                WHEN machine_name = \'excessive_over_speed\' THEN 1 \n\
                ELSE 0 \n\
             END AS excessive_over_speed_event\n\
          FROM \n\
            prod.orion.vehicle_events v\n\
          WHERE 1=1\n\
            AND v.logged_at > dateadd(day, -30, convert_timezone(\'UTC\', current_timestamp))\n\
            AND v.logged_at <= convert_timezone(\'UTC\', current_timestamp)\n\
        )\n\
            SELECT\n\
                b.environment,\n\
                b.asset_id,\n\
                b.weekday,\n\
                b.date_hour,\n\
                b.date_day,\n\
                b.date_month,\n\
                b.hard_brake_event,\n\
                b.emergency_braking_active_event,\n\
                b.excessive_over_speed_event\n\
           FROM \n\
              baseline b\n\
           INNER JOIN enabled_cnt c ON (\n\
                b.environment = c.environment\n\
                AND b.date_day = c.date_day\n\
              )\n\
        LIMIT {};'.format(LIMIT)
    execute_query(connection=snowflake_conn, query=emergency_braking_sql_cmd,
                  pandas=True)

def analyze_heartbeat_sessions():
    """
    Heartbeat sessions.
    Returns a list of tuples like:
    DEPLOYMENT_ID, POWER_UNIT_VIN, ESN, TABLET_SERIAL, CVD_TYPE, SESSION_TYPE,
    START_TIME, END_TIME
    """
    sql_cmd = 'SELECT * from "PROD"."HEARTBEAT_SESSIONS"."IGNITION" '\
              'LIMIT {};'.format(LIMIT)
    rsp = execute_query(connection=snowflake_conn, query=sql_cmd)
    plot_data(panda_dataframe=rsp,
              data_dict={'kind': 'line',
                         'x-axis': 'ESN',
                         'y-axis': ['START_TIME', 'END_TIME']},
              show_plot=True,
              title_name='Heartbeat session')

def heartbeat_vin_decode():
    """
    VIN decode to get make/model/year.
    ESN, EE_VERSION, CONFIG_VERSION, DATE_HOUR,
    TRUCK_MAKE, TRUCK_MODEL, TRUCK_MODEL_YEAR
    """

    # FROM orion.telematics_heartbeat \n\
    # Unfortunately USX is special in that it only writes data to Timescale,
    # and for now that data is only available in a different table, specifically:
    # orion.heartbeats_timescale

    hb_vin_decode_sql_cmd = \
        'WITH hb as (\n\
            SELECT\n\
                doc:entities:power_unit:vin::VARCHAR as power_unit_vin,\n\
                doc:entities:power_unit:power_unit_id as power_unit_id,\n\
                doc:entities:cvd:esn as esn,\n\
                doc:device_info:cvd_sw:engine_events_ipk:version AS ee_version,\n\
                doc:device_info:cvd_sw:upm_version:version as upm_version,\n\
                doc:device_info:cvd_sw:lmu_firmware:version AS lmu_fw,\n\
                doc:device_info:phone_number AS phone_number,\n\
                doc:debug:config_version as config_version,\n\
                date(created_at) as date_hour\n\
            FROM orion.heartbeats_timescale\n\
            WHERE \n\
                utils.deployment_id_to_customer_name(deployment_id) = \'{}\'\n\
                AND created_at between \'{}\' and \'{}\'\n\
            GROUP BY date_hour, power_unit_id, esn, lmu_fw, ee_version, upm_version, config_version, power_unit_vin \n\
                -- , truck_make, truck_model, truck_model_year\n\
        ), hb_vin_decode as (\n\
            SELECT \n'.format(FILTER_BY_ENVIRONMENT, START_DATE, END_DATE)

    if RELEASE_MONITOR_DISPLAY['Pwr-ID'] is True:
        hb_vin_decode_sql_cmd += '\t\thb.power_unit_id as power_unit_id'
    if RELEASE_MONITOR_DISPLAY['ESN'] is True:
        hb_vin_decode_sql_cmd += ',\n\t\thb.esn as esn'
    if RELEASE_MONITOR_DISPLAY['Phone'] is True:
        hb_vin_decode_sql_cmd += ',\n\t\thb.phone_number as phone_number'
    if RELEASE_MONITOR_DISPLAY['EE'] is True:
        hb_vin_decode_sql_cmd += ',\n\t\thb.ee_version as ee_version'
    if RELEASE_MONITOR_DISPLAY['UPM'] is True:
        hb_vin_decode_sql_cmd += ',\n\t\thb.upm_version as upm_version'
    if RELEASE_MONITOR_DISPLAY['LMU-FW'] is True:
        hb_vin_decode_sql_cmd += ',\n\t\thb.lmu_fw as lmu_fw'
    if RELEASE_MONITOR_DISPLAY['Config Version'] is True:
        hb_vin_decode_sql_cmd += ',\n\t\thb.config_version as config_version'
    if RELEASE_MONITOR_DISPLAY['Timestamp Rx'] is True:
        hb_vin_decode_sql_cmd += ',\n\t\thb.date_hour as date_hour'
    if RELEASE_MONITOR_DISPLAY['Make'] is True:
        hb_vin_decode_sql_cmd += ',\n\t\tv.doc:Make::VARCHAR as truck_make'
    if RELEASE_MONITOR_DISPLAY['Model'] is True:
        hb_vin_decode_sql_cmd += ',\n\t\tv.doc:Model::VARCHAR as truck_model'
    if RELEASE_MONITOR_DISPLAY['Year'] is True:
        hb_vin_decode_sql_cmd += ',\n\t\tv.doc:ModelYear::VARCHAR as truck_model_year'
    hb_vin_decode_sql_cmd += '\n\
            FROM hb\n\
            LEFT JOIN user_pwatts.vin_decode v on hb.power_unit_vin = v.doc:VIN\n\
        )\n'

    hb_vin_decode_sql_cmd += '\tSELECT * FROM hb_vin_decode LIMIT {};'.format(LIMIT)

    execute_query(connection=snowflake_conn, query=hb_vin_decode_sql_cmd,
                  pandas=True)

def analyze_telematics_heartbeat(print_debug_info=False,
                                 query_element=None,
                                 start_date=None,
                                 end_date=None):
    """
    Analyze heartbeat
    Elements: ID, DEPLOYMENT_ID, TELMATICS_GROUP_ID, EVENT, FUEL_LEVEL, GEO,
              LOCATION_DESCRIPTION, CITY, STATE_CODE, COUNTRY_CODE, ODOMETER,
              ODOMETER_GROUP, ODOMETER_JUMP, IGNITION, RPM, ENGINE_HOURS,
              ENGINE_HOURS_JUMP, WHEELS_IN_MOTION, SPEED, HEADING, ACCURACY,
              SATELLITES, HDOP, GPS_VALID SOURCE, STATE_CODE_ABSOLUTE,
              TOTAL_FUEL_USED, TRIP_ID, HAS_IDLE_PERIOD, ASSET_ID, USER_ID,
              TABLET_ID, LOGGED_AT, CREATED_AT, UPDATED_AT, UUID

            logged_at = time heartbeat created on CVD
            created_at = time we receive the heartbeat
    """
    # Access elements from within a JSON block:
    # query_element = 'doc:entities:cvd:esn as esn'  # Select
    # query_element = 'doc:entities:cvd:esn as esn,ODOMETER,FUEL_LEVEL,ID'
    sql_cmd = '\nSELECT \n'
    if query_element:
        sql_cmd += '{} from "PROD"."ORION"."TELEMATICS_HEARTBEAT" \n'.format(
                query_element)
    else:
        sql_cmd += '* from "PROD"."ORION"."TELEMATICS_HEARTBEAT" \n'
    if start_date and end_date:
        sql_cmd += 'WHERE created_at BETWEEN \'{}\' and \'{}\' \n'.format(
                start_date, end_date)
    if FILTER_BY_ENVIRONMENT != '':
        # Quality of life function to not need to look up deployment ID per environment
        if start_date and end_date:
            sql_cmd += 'AND \n'
        else:
            sql_cmd += 'WHERE \n'
        sql_cmd += 'utils.deployment_id_to_customer_name(deployment_id) '\
                   '= \'{}\' \n'.format(FILTER_BY_ENVIRONMENT)
    sql_cmd += 'LIMIT {};'.format(LIMIT)
    rsp = execute_query(connection=snowflake_conn, query=sql_cmd)
    plot_data(panda_dataframe=rsp,
              data_dict={'kind': 'line',
                         'x-axis': 'TOTAL_FUEL_USED',
                         'y-axis': ['ENGINE_HOURS']},
              show_plot=True,
              title_name='Telematics Heartbeat')
    parse_debug_data_from_heartbeat(
            data_frame_object=rsp, print_debug_info=print_debug_info)


def generate_list_esns_release():
    """
    """
    WD = 'esn_files/generate_list_of_esns/'
    FILE_OF_ESNS = 'esn_files/generate_list_of_esns/list_of_esns.csv'
    FILE_OF_ESNS_PULS_ON_41G = 'esn_files/generate_list_of_esns/list_of_esns_on_41g_puls.csv'
    FILE_OF_ESNS_EXCLUDED_EXCLUSIONS = 'esn_files/generate_list_of_esns/list_of_esns_excluded.csv'
    EXCLUSION_LIST = 'esn_files/generate_list_of_esns/exclusion_list.csv'
    PULS_CSV_LIGHT_FILE = WD + 'devices.csv'
    filtered_list_to_display = monitor_release()

    # Start with a clean slate
    if os.path.exists(FILE_OF_ESNS):
        os.remove(FILE_OF_ESNS)
    if os.path.exists(FILE_OF_ESNS_PULS_ON_41G):
        os.remove(FILE_OF_ESNS_PULS_ON_41G)

    # Filter out jpod configs
    list_of_jpod_configs_to_remove = ['58', '59', '156', '158', '169', '55']
    jpod_config_index = -1
    branch_name_index = -1
    for index, element in enumerate(filtered_list_to_display[0]):
        if element == "J-Cfg":
            jpod_config_index = index
        elif element == "Branch":
            branch_name_index = index
    print("Now removing any device with a jpod config of {}".format(list_of_jpod_configs_to_remove))
    print("Also removing any branch name not equal to hsl_master\n")
    # what about release/VHCL-751, feature/VHCL-751-2022-Kenworth
    number_of_devices_removed = 0
    list_of_esns_filtered_jpod_configs = list()
    for line in filtered_list_to_display:
        if line[jpod_config_index] not in list_of_jpod_configs_to_remove:
            list_of_esns_filtered_jpod_configs.append(line)
        elif line[branch_name_index] == "hsl_master":
            list_of_esns_filtered_jpod_configs.append(line)
        else:
            print("removing {}".format(line))
            number_of_devices_removed += 1
    print("\nRemoved {} devices.".format(number_of_devices_removed))

    if len(list_of_esns_filtered_jpod_configs) > 1:
        length = len(list_of_esns_filtered_jpod_configs[1])
    else:
        print("Done.")
        os._exit(0)
    with open(FILE_OF_ESNS, "w") as file:
        file.write('ESN      \tPower_Unit_Id\tDevice\r\n')
        for line in list_of_esns_filtered_jpod_configs[1:]:
            # Just save Power Unit ID, ESN, and Device Type
            string_to_write = str(line[1]) + '\t' +\
                              str(line[2]) + '\t\t\t' +\
                              str(line[length - 2])
            file.write(string_to_write)
            file.write("\r\n")

    if os.path.exists(FILE_OF_ESNS_EXCLUDED_EXCLUSIONS):
        os.remove(FILE_OF_ESNS_EXCLUDED_EXCLUSIONS)

    print("\nNow removing any devices from exclusion list...")
    compare_list_to_exclusion(
        fresh_file=FILE_OF_ESNS_EXCLUDED_EXCLUSIONS,
        exclusion_list=EXCLUSION_LIST,
        file_to_compare=FILE_OF_ESNS)

    if os.path.exists(FILE_OF_ESNS_EXCLUDED_EXCLUSIONS):
        os.system('mv {} {}'.format(FILE_OF_ESNS_EXCLUDED_EXCLUSIONS, FILE_OF_ESNS))
    with open(file=FILE_OF_ESNS,
              encoding="utf-8", mode='r') as file:
        lines = file.readlines()
    num_devices = 0
    print("\nUpdated ESN list is here: \n")
    for row in lines[1:]:
        print("{}".format(row[0:10]))
        num_devices += 1
    print("{} devices.".format(num_devices))

    print("\nNext we need to remove any devices already on 4.1G...")
    print("Using ESNs from above, extract CSV Light from PULS and copy to {}".format(WD))
    print("Enter file name of PULS export... expect devices.csv ")
    user_command = input()

    list_of_esns_on_41g = list()
    if os.path.exists(PULS_CSV_LIGHT_FILE):
        print("Found {}".format(PULS_CSV_LIGHT_FILE))
        try:
            with open(file=PULS_CSV_LIGHT_FILE,
                      encoding="utf-8", mode='r') as file:
                lines = file.readlines()
        except IsADirectoryError:
            print("Not a valid file!!\n\n")
            os._exit(-1)
        list_of_ens_on_41g = list()
        for line in lines[1:]:
            # Header = "MOBILE ID","CUSTOMER","GROUP","STATUS","REV",
            # "APP","CLASS","SCRIPT","CONFIG","LAST ID REPORT","ESN",...
            # Save ESN and firmware version ("REV")
            test = line.split('""')[0].split(',')
            fw_version = test[4].replace('"', '')
            if fw_version == '41g':
                str_to_save = test[10].replace('"', "")
                str_to_save = str_to_save.replace("=", "")
                list_of_ens_on_41g.append(str_to_save)
        print("PULS found devices on 41g: {}".format(FILE_OF_ESNS_PULS_ON_41G))
        with open(FILE_OF_ESNS_PULS_ON_41G, "w") as file:
            for line in list_of_ens_on_41g:
                file.write(line)
                file.write("\r\n")
    else:
        print("Did NOT find {}".format(PULS_CSV_LIGHT_FILE))
        os._exit(-1)

    compare_list_to_exclusion(
        fresh_file=FILE_OF_ESNS_EXCLUDED_EXCLUSIONS,
        exclusion_list=FILE_OF_ESNS_PULS_ON_41G,
        file_to_compare=FILE_OF_ESNS)
    if os.path.exists(FILE_OF_ESNS_EXCLUDED_EXCLUSIONS):
        os.system('mv {} {}'.format(FILE_OF_ESNS_EXCLUDED_EXCLUSIONS, FILE_OF_ESNS))
    print("\nUpdated ESN list is here: {}\n".format(FILE_OF_ESNS))


def analyze_heartbeat_data():
    """
    Analyze heartbeat models from flattened timescale to have easier access.
    NOTE: These are not maintained and may be out of date!
    Optional elements to analyze:
    ['HEARTBEAT_ID', 'DEPLOYMENT_ID', 'CVD_ID', 'ESN', 'TABLET_ID',
     'ASSET_ID', 'USER_ID', 'LATITUDE', 'LONGITUDE', 'WHEELS_IN_MOTION',
     'SPEED', 'ODOMETER', 'ODOMETER_JUMP', 'TOTAL_FUEL_USED', 'FUEL_LEVEL',
     'HDOP', 'RPM', 'EVENT', 'TRIP_ID', 'SHUTDOWN_REASON', 'IGNITION',
     'FLAGS', 'VBUS_STATUS', 'VEHICLE_BUS_AVAILABLE', 'CVD_UPTIME',
     'DEVICE_TYPE', 'ICCID', 'PHONE_NUMBER', 'IP_ADDRESS', 'CVD_VOLTAGE',
     'ENGINE_STATUS', 'DTNA_ANALYTICS_VERSION', 'GO_VEHICLE_VERSION', 'RSSI',
     'POWER_UNIT_VIN', 'POWER_UNIT', 'TABLET_SERIAL', 'TABLET_HEALTH',
     'TABLET_BATTERY_LEVEL', 'TABLET_PLUGGED', 'TABLET_PRESENT',
     'TABLET_SCALE', 'TABLET_TECH', 'TABLET_TEMP', 'TABLET_VOLTAGE',
     'WIFI_AP_CHAN', 'WIF_INFO', 'WIFI_POWER', 'WIFI', 'ENGINE_EVENTS_MEM_MB',
     'HSL_MEM_MB', 'MEMORY_MITIGATION', 'SOCKET_MITIGATION',
     'MEMORY_IGN_OFF_MITIGATION', 'TABLET_SOCKET', 'WIFI_STATS_TIMESTAMP_UTC',
     'SELECTED_TOTAL_FUEL_SA', 'SELECTED_TOTAL_FUEL_SPN',
     'SELECTED_TOTAL_FUEL_VALID', 'SELECTED_TOTAL_ODOMETER_SA',
     'SELECTED_ODOMETER_SPN', 'SELECTED_ODOMETER_VALID', 'STORE_AND_FORWARD']
    """
    sql_cmd = 'SELECT * FROM "PROD"."HEARTBEAT_MODELS"."FLATTENED" '\
              'WHERE created_at > dateadd(day, -1, current_timestamp) '\
              'LIMIT {};'.format(LIMIT)
    rsp = execute_query(connection=snowflake_conn,
                        query=sql_cmd)

    plot_data(panda_dataframe=rsp,
              data_dict={'kind': 'line',
                         'x-axis': 'ESN',
                         'y-axis': ['ODOMETER', 'CVD_UPTIME']},
              show_plot=False,
              title_name='Odometer stuff')

    plot_data(panda_dataframe=rsp,
              data_dict={'kind': 'line',
                         'x-axis': 'ESN',
                         'y-axis': ['TOTAL_FUEL_USED']},
              show_plot=False,
              title_name='Fuel level stuff')

    plot_data(panda_dataframe=rsp,
              data_dict={'kind': 'line',
                         'x-axis': 'ENGINE_STATUS',
                         'y-axis': ['CVD_VOLTAGE', 'WHEELS_IN_MOTION']},
              show_plot=True,
              title_name='Engine Status stuff')

def paccar_analysis_sa_vin():
    
    sql_cmd = "with heartbeats as (

        SELECT
            -- doc,
            doc:entities:power_unit:vin::VARCHAR(16777216)                          AS power_unit_vin,
            v.make,
            v.manufacturer,
            v.model_year,
            parse_json(doc:debug:engine_data_validator.multiple_sa)::variant 		as multiple_sa_json,
            OBJECT_KEYS(multiple_sa_json:"odometer HR, spn:917")[0]::number 		as first_sa,
            OBJECT_KEYS(multiple_sa_json:"odometer HR, spn:917")[1]::number			as second_sa,
            OBJECT_KEYS(multiple_sa_json:"odometer LR, spn:245")[0]::number			as third_sa,
            OBJECT_KEYS(multiple_sa_json:"odometer LR, spn:245")[1]::number			as fourth_sa,
            trip_id,
            city,
            created_at,
            logged_at,
            deployment_id
        from 
            "PROD"."ORION"."TELEMATICS_HEARTBEAT"
        left join utils.vins_decoded_flattened v on v.vin = power_unit_vin
        WHERE 
            1=1
            AND logged_at > dateadd(day, -1, current_timestamp)
            and logged_at < current_timestamp
            AND doc:telematics:engine_status = 'driving'
            AND v.model_year >= '2020'
            AND make in ('KENWORTH', 'PETERBILT')
    )
    select 
    //	manufacturer,
        model_year,
        first_sa,
        second_sa,
        third_sa,
        fourth_sa,
        count(*)
    from
        heartbeats
    GROUP BY 1, 2,3,4,5
    ORDER BY 5 desc
    LIMIT 1000;"

def monitor_release_fast():
    """
    Given an ESN list and several parameters, display import version information.
    Exits with failure state if mac address is default or not all ESNs
    reported in via heartbeat with given filters.
    Elements: ID, DEPLOYMENT_ID, TELMATICS_GROUP_ID, EVENT, FUEL_LEVEL, GEO,
              LOCATION_DESCRIPTION, CITY, STATE_CODE, COUNTRY_CODE, ODOMETER,
              ODOMETER_GROUP, ODOMETER_JUMP, IGNITION, RPM, ENGINE_HOURS,
              ENGINE_HOURS_JUMP, WHEELS_IN_MOTION, SPEED, HEADING, ACCURACY,
              SATELLITES, HDOP, GPS_VALID SOURCE, STATE_CODE_ABSOLUTE,
              TOTAL_FUEL_USED, TRIP_ID, HAS_IDLE_PERIOD, ASSET_ID, USER_ID,
              TABLET_ID, LOGGED_AT, CREATED_AT, UPDATED_AT, UUID

            logged_at = time heartbeat created on CVD
            created_at = time we receive the heartbeat

    Can be called with the following arguments:
       Search by dates      : --start=2021-10-01 --end=2021-10-11
       Search by environment: --filter_by_environment=WMT
       Search by jpod cfg   : --filter_by_jpod_config=56
       Search by jpod fw    : --filter_by_jpod_fw=12q
       Search by UPM        : --filter_by_upm=3.22.25-286
       Search by EE         : --filter_by_ee=5.12.0-147
       Search by type       : --filter_by_device_type=LMU5541
       Search by LMU CFG    : --filter_by_lmu_cfg=55.50
       Search by LMU FW     : --filter_by_lmu_fw=V4.1d
       Search by ESN        : --filter_by_esn=5572004945
       Search by Trip ID    : --filter_by_trip_id=26424
       Search by HaPy       : --filter_by_hapy=1.1.7-16
       Search list of ESNS  : --file_of_esns=41F_Beta_VVG.csv
       Use DEV environement : --use_dev_env=yes
       Use INT environement : --use_int_env=yes
       Use UAT environement : --use_uat_env=yes
    """
    def print_table(rows):
        max_widths = []
        for column in zip(*rows):
            max_widths.append(max([len(text) for text in column]))
        template = '  '.join(['{{:<{}}}'.format(width) for width in max_widths])
        return '\n'.join([template.format(*row) for row in rows])

    list_of_requested_esns = []
    list_of_requested_power_units = []
    list_of_heartbeated_esns = []

    start_time = datetime.now()

    SEARCH_LAST_HOUR = False
    CURRENT_DATE = (datetime.now()).strftime("%Y-%m-%d").replace('-0', '-')
    global START_DATE
    START_DATE = str(START_DATE).replace('-0', '-')
    if CURRENT_DATE == START_DATE:
        SEARCH_LAST_HOUR = True


    sql_cmd = '\nWITH ee_heartbeats as (\n'
    sql_cmd += '  SELECT\n'
    sql_cmd += '    deployment_id,\n'
    sql_cmd += '    heartbeat_id\n'
    sql_cmd += '  from\n'
    sql_cmd += '    heartbeat_models.flattened\n'
    if START_DATE and END_DATE and not SEARCH_LAST_HOUR:
        sql_cmd += 'WHERE created_at BETWEEN \'{}\' and \'{}\' \n'.format(
                START_DATE, END_DATE)
    else:
        sql_cmd += "WHERE \n"
    if SEARCH_LAST_HOUR:
        sql_cmd += 'CREATED_AT > current_timestamp - INTERVAL \'35 minutes\'\n'  # '1 hour'
    extra_filters = filter_for_certain_values_fast()
    sql_cmd += extra_filters

    sql_cmd += '  )\n'
    sql_cmd += ', detailed_heartbeats as (\n'
    sql_cmd +=     'SELECT\n'
    sql_cmd +=         'doc,\n'
    sql_cmd +=         'trip_id,\n'
    sql_cmd +=         'city,\n'
    sql_cmd +=         'created_at,\n'
    sql_cmd +=         'logged_at,\n'
    sql_cmd +=         'e.deployment_id\n'
    sql_cmd +=     'FROM\n'
    sql_cmd +=         'orion.telematics_heartbeat th\n'
    sql_cmd +=         'INNER JOIN ee_heartbeats e on '
    sql_cmd += '(th.deployment_id = e.deployment_id and th.id = e.heartbeat_id)\n'
    sql_cmd += ')\n'
    sql_cmd += 'SELECT * FROM detailed_heartbeats\n'

    if FILE_OF_ESNS != '':
        try:
            with open(file=FILE_OF_ESNS, encoding="utf-8", mode='r') as file:
                lines = file.readlines()
            sql_cmd += "AND doc:entities:cvd:esn in (\n"
            for line_item in lines[:-1]:
                esn_value = line_item.replace("\n", "")
                list_of_requested_esns.append(esn_value)
                sql_cmd += "'{}',\n".format(esn_value)
            list_of_requested_esns.append(lines[-1].replace("\n", ""))
            # Delete comma
            sql_cmd += "'{}'\n".format(list_of_requested_esns[-1])
            sql_cmd += ")\n"
        except Exception as exception:
            sys.stdout.write("\t\t\tError occurred! {}\n\n".format(exception))
    elif FILE_OF_DRIVER_IDS != '':
        try:
            with open(file=FILE_OF_DRIVER_IDS,
                    encoding="utf-8", mode='r') as file:
                lines = file.readlines()
            sql_cmd += "AND doc:entities:user:external_id in (\n"
            for line_item in lines[:-1]:
                driver_value = line_item.replace("\n", "")
                list_of_requested_esns.append(driver_value)
                sql_cmd += "'{}',\n".format(driver_value)
            list_of_requested_esns.append(lines[-1].replace("\n", ""))
            # Delete comma
            sql_cmd += "'{}'\n".format(list_of_requested_esns[-1])
            sql_cmd += ")\n"
        except Exception as exception:
            sys.stdout.write("\t\t\tError occurred! {}\n\n".format(exception))
    elif FILE_OF_POWER_UNIT_IDS != '':
        try:
            with open(file=FILE_OF_POWER_UNIT_IDS,
                      encoding="utf-8", mode='r') as file:
                lines = file.readlines()
            sql_cmd += "AND doc:entities:power_unit:power_unit_id in (\n"
            for line_item in lines[:-1]:
                esn_value = line_item.replace("\n", "")
                list_of_requested_power_units.append(esn_value)
                sql_cmd += "'{}',\n".format(esn_value)
            list_of_requested_power_units.append(lines[-1].replace("\n", ""))
            # Delete comma
            sql_cmd += "'{}'\n".format(list_of_requested_power_units[-1])
            sql_cmd += ")\n"
        except Exception as exception:
            sys.stdout.write("\t\t\tError occurred! {}\n\n".format(exception))
    elif FILTER_BY_ESN != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:entities:cvd:esn in ('{}') \n".format(FILTER_BY_ESN)
    elif FILTER_BY_TRACTOR_ID != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:entities:power_unit:power_unit_id in ('{}') \n".\
                format(FILTER_BY_TRACTOR_ID)
    elif FILTER_BY_VIN != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:entities:power_unit:vin in ('{}') \n".\
                format(FILTER_BY_VIN)
    elif FILTER_BY_TABLET_SERIAL != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:entities:tablet:serial in ('{}') \n".\
                format(FILTER_BY_TABLET_SERIAL)
    elif FILTER_BY_USER_EXTERNAL_ID != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:entities:user:external_id in ('{}') \n".\
                format(FILTER_BY_USER_EXTERNAL_ID)
    elif FILTER_BY_USER_ID != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:entities:user:id in ('{}') \n".format(FILTER_BY_USER_ID)
    if FILTER_BY_TRIP_ID != '':
        sql_cmd += "AND \n"
        sql_cmd += "CHARINDEX('{}', trip_id) > 0 \n".format(FILTER_BY_TRIP_ID)
    if FILTER_BY_CITY != '':
        sql_cmd += "AND \n"
        sql_cmd += "city in ('{}') \n".format(FILTER_BY_CITY)
    if FILTER_BY_UPM != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:upm_version:version='{}' \n".\
            format(FILTER_BY_UPM)
    if FILTER_BY_JPOD_CONFIG != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:jpod_config:version='{}' \n".\
            format(FILTER_BY_JPOD_CONFIG)
    if FILTER_BY_JPOD_FW != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:jpod_firmware:version='{}' \n".\
            format(FILTER_BY_JPOD_FW)
    if FILTER_BY_DEVICE_TYPE != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:device_type='{}' \n".\
            format(FILTER_BY_DEVICE_TYPE)
    if FILTER_BY_LMU_CFG != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:lmu_config:version='{}' \n".\
            format(FILTER_BY_LMU_CFG)
    if FILTER_BY_LMU_FW != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:lmu_firmware:version='{}' \n".\
            format(FILTER_BY_LMU_FW)
    if FILTER_BY_HAPY != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:ha_python_api_ipk:version='{}' \n".\
            format(FILTER_BY_HAPY)
    if FILTER_BY_EE != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:engine_events_ipk:version='{}' \n".\
            format(FILTER_BY_EE)

#    extra_filters = filter_for_certain_values()
#    sql_cmd += extra_filters

    sql_cmd += 'LIMIT {};\n'.format(LIMIT)
    rsp = execute_query(connection=snowflake_conn,
                        query=sql_cmd,
                        display_response=False)

    parsed_results = get_list_of_formatted_data(df=rsp)
    save_results_to_file(response=parsed_results, filename_prefix="HB",
            sort_keyword="CREATED_AT")
    list_to_use = list_of_requested_esns
    if FILE_OF_ESNS != "":
        list_to_use = list_of_requested_esns
        key_id_to_use = "ESN"
    elif FILE_OF_POWER_UNIT_IDS != "":
        list_to_use = list_of_requested_power_units
        key_id_to_use = "POWER_UNIT_ID"
    elif FILE_OF_DEVICE_IDS != "":
        list_to_use = list_of_requested_device_ids 
        key_id_to_use = "DEVICE_ID"

    if len(list_to_use) > 0:
        sys.stdout.write("Expected {} unique responses\n".format(
            len(list_to_use)))
    if len(list_to_use) - len(parsed_results) > 0:
        sys.stdout.write("\nMissing {} ({}):\n".format(
            key_id_to_use, len(list_to_use) - len(parsed_results)))
        for esn in list_to_use:
            if not parsed_results['{}'.format(
                    key_id_to_use)].str.contains(esn).any():
                sys.stdout.write("\t{}\n".format(esn))
        sys.stdout.write("\n")  # single '\n', not x2

    end_time = datetime.now()
    hours_min_seconds = str(
            timedelta(seconds=(end_time - start_time).seconds))
    sys.stdout.write("Response time: {} ( {} )\n\n".format(
        end_time, hours_min_seconds))

def filter_for_certain_values_fast():
    """
    Only get results with certain values
    """
    filtered_sql_cmd = ''

    # --------------------- Search for speed issues ----------------------:
    SEARCH_SPEED_SENSOR_FAILURES = False
    if SEARCH_SPEED_SENSOR_FAILURES:
        filtered_sql_cmd += "AND doc:debug:sensor_failures:speed.cause = "\
            "'GPS says truck is in motion and the engine is ON, but wheel "\
            "speed value read is 0'\n"

    FILTER_SPECIFIC_EE_BRANCH = False
    if FILTER_SPECIFIC_EE_BRANCH:
        interesting_ee_branch_list = [
            "hsl_master",
            "release/5.21.5-Paccar"
            ]
        filtered_sql_cmd += "AND doc:device_info:cvd_sw:engine_events_ipk:"\
                "branch in (\n"
        for version in interesting_ee_branch_list[:-1]:
            filtered_sql_cmd += "\t'{}',\n".format(version)
        filtered_sql_cmd += "\t'{}')\n".format(interesting_ee_branch_list[-1])

    FILTER_SPECIFIC_EE_VERSION = False
    if FILTER_SPECIFIC_EE_VERSION:
        filtered_sql_cmd += "AND engine_events_ipk_version not LIKE '5.22.8-%'\n"

    FILTER_SPECIFIC_JPOD_VERSIONS = False
    if FILTER_SPECIFIC_JPOD_VERSIONS:
        interesting_jpod_config_list = [
                "97",
                "98"
                ]
        filtered_sql_cmd += "AND jpod_config_version in (\n"
        for version in interesting_jpod_config_list[:-1]:
            filtered_sql_cmd += "\t'{}',\n".format(version)
        filtered_sql_cmd += "\t'{}')\n".format(interesting_jpod_config_list[-1])

    FILTER_SPECIFIC_UPM_VERSIONS = True
    if FILTER_SPECIFIC_UPM_VERSIONS:
        interesting_upm_list = [
                "3.52.255-188",
                "3.52.256-189",
                "3.52.257-190",
                "3.52.258-191",
                "3.52.259-192",
                "3.52.260-193",
                "3.52.261-194",
                "3.52.262-195",
                "3.52.263-196",
                "3.52.264-197",
                "3.52.265-198",
                "3.52.266-199",
                "",
                ]
        filtered_sql_cmd += "AND upm_version_version in (\n"
        for version in interesting_upm_list[:-1]:
            filtered_sql_cmd += "\t'{}',\n".format(version)
        filtered_sql_cmd += "\t'{}')\n".format(interesting_upm_list[-1])

    FILTER_NEGATIVE_FUEL_LEVEL = False
    if FILTER_NEGATIVE_FUEL_LEVEL:
        filtered_sql_cmd += 'AND doc:telematics:fuel_level < 0\n'

    # -------------------------- Search total_fuel_used ------------:
    #filtered_sql_cmd += "AND doc:telematics:total_fuel_used != 16202.5\n"
    #filtered_sql_cmd += "AND doc:telematics:total_fuel_used != 37134.1\n"

    # -------------------------- Search 4.1k increased reboots ------------:
    FILTER_SPECIFIC_FW_VERSIONS = False
    if FILTER_SPECIFIC_FW_VERSIONS:
        interesting_fw_list = [
                "V4.1i",
                ]
        filtered_sql_cmd += "AND lmu_firmware_version in (\n"
        for version in interesting_fw_list[:-1]:
            filtered_sql_cmd += "\t'{}',\n".format(version)
        filtered_sql_cmd += "\t'{}')\n".format(interesting_fw_list[-1])

    FILTER_SPECIFIC_SHUTDOWN_REASON = False
    if FILTER_SPECIFIC_SHUTDOWN_REASON:
        #filtered_sql_cmd += "AND doc:debug:shutdown_reason != ''\n"
        interesting_shutdown_reason_list = [
            'unexpected CVD reboot',
            'periodic reboot',
            'cell issue EE restart',
            'exception no reason found.',
            'sigterm',
            'one shot sleep timer called',
            'rebooting cvd due to stuck jpod or fuel issues'
            ]
        filtered_sql_cmd += "AND shutdown_reason"\
                " in (\n"
        for version in interesting_shutdown_reason_list[:-1]:
            filtered_sql_cmd += "\t'{}',\n".format(version)
        filtered_sql_cmd += "\t'{}')\n".format(
                interesting_shutdown_reason_list[-1])

    FILTER_SHERLOCK_LMU_MONITOR_CRASHED = False
    if FILTER_SHERLOCK_LMU_MONITOR_CRASHED: 
        filtered_sql_cmd += "AND SYSTEM_ERROR_LOG LIKE "\
                "'%calamp_lmu_monitor: cmm has terminated%'\n"

    return filtered_sql_cmd

