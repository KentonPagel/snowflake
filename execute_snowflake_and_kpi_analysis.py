"""
Execute snowflake and kpi analsyis
"""
#!/usr/bin/python3

import os
import sys
import signal
import select
import time
from datetime import datetime
import subprocess
from threading import Thread
import argparse
import shlex
import snowflake.connector as snow_conn

def MINUTES(minutes=0):
    """
    Convert minutes to seconds
    """
    return 60.0 * minutes

def parse_arguments():
    """
    Parse arguments from command line, return them.
    """
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--file_of_esns',
            help="Name of file with list of ESNs")

    arguments = arg_parser.parse_args()
    if arguments.file_of_esns:
        list_of_requested_esns = []
        try:
            with open(file=arguments.file_of_esns,
                    encoding="utf-8", mode='r') as file:
                lines = file.readlines()
            for line_item in lines[:-1]:
                esn_value = line_item.replace("\n", "")
                list_of_requested_esns.append(esn_value)
            list_of_requested_esns.append(lines[-1].replace("\n", ""))
            return list_of_requested_esns
        except Exception as exception:
            print("\t\t\tError occurred! {}\n\n".format(exception))
    else:
        print("Need to pass in a file of esns...")
        sys.exit(-1)


def execute_subprocess(command=''):
    """
    TODO
    """
    time_seconds = MINUTES(5)
    with subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False) as snow_status:
        poll_obj = select.poll()
        poll_obj.register(snow_status.stdout, select.POLLIN)
        poll_obj.register(snow_status.stderr, select.POLLIN)
        os.set_blocking(snow_status.stderr.fileno(), False)
        while True:
            poll_result = poll_obj.poll(0)
            output = []
            output_error = []
            readline_attemps = 0
            MAX_ATTEMPTS = 5000
            while poll_result:
                if readline_attemps > MAX_ATTEMPTS:
                    readline_attemps += 1
                    break
                readline_attemps += 1
                readline_result = snow_status.stdout.readline()
                readline_result_error = snow_status.stderr.readline()
                output_error.append(str(readline_result_error))
                output.append(readline_result)
                poll_result = poll_obj.poll(0)
                # time.sleep(0.05)
            if readline_attemps > MAX_ATTEMPTS and len(output) == 0:
                output_error.append(snow_status.stderr.readline())
                print("Something went wrong! Reset nightrider exe?: \n")
                for row in output_error:
                    if "b''" != str(row):
                        print("{}".format(str(row)))
                sys.exit(-1)
            else:
                for row in output:
                    if b'Response time' in row:
                        time_seconds = 0
                        break
            time.sleep(1.0)
            if time_seconds > 0:
                time_seconds -= 1.0
            elif time_seconds <= 0.0:
                snow_status.kill()
                break

def execute_snowflake(esn=0):
    """
    TODO
    """
    # TODO, limit, dates
    command_template = '/bin/bash -c "source {}/bin/activate && python3 snowflake_query.py --query_type=monitor_release --start_date=2023-12-10 --end_date=2023-12-15 --filter_by_esn={} --limit=50"'
    command = shlex.split(command_template.format("snowflake_venv", esn))
    execute_subprocess(command=command)


def execute_kpi_analysis():
    """
    TODO
    """
    command_template = '/bin/bash -c "source {}/bin/activate && python3 kpi_analysis_single_esn_history.py --file_for_kpi_analysis=results_HB_71322_5571089164.csv"'
    command = shlex.split(command_template.format("snowflake_venv"))
    execute_subprocess(command=command)


if __name__ == '__main__':
    list_of_requested_esns = parse_arguments()

    print("We have {} ESNs to search..".format(len(list_of_requested_esns)))
    for esn in list_of_requested_esns:
        print("Getting HB snowflake data for ESN: {} at {}".format(esn, datetime.now()))
        execute_snowflake(esn=esn)

    print("Now perform KPI analysis...{}".format(datetime.now()))
    execute_kpi_analysis()

