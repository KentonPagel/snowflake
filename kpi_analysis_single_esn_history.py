#!/usr/bin/python3
"""
KPI Analysis on a single ESN!
"""

import os
import sys
import csv
import argparse

FILE_FOR_KPI_ANALYSIS = ''
LIST_OF_FILES_FOR_KPI_ANALYSIS = []
LIST_OF_KPI_ERRORS = []
KPI_ERRORS_FILENAME = "results_KPI_Errors_Detected.txt"

def setup():
    """
    Things to do to set up environment.
    """
    # NOTE: allow non distorted view on cmd line
    os.system('tput rmam')

def teardown():
    """
    Things to do to set up environment.
    """
    # NOTE: allow non distorted view on cmd line
    os.system('tput smam')

def parse_arguments():
    """
    Parse arguments from command line, return them.
    """
    global FILE_FOR_KPI_ANALYSIS
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--file_for_kpi_analysis',
                            help="File of HB history on a single ESN")

    arguments = arg_parser.parse_args()
    if arguments.file_for_kpi_analysis is not None:
        FILE_FOR_KPI_ANALYSIS = arguments.file_for_kpi_analysis

def kpi_analysis(kpi_filename=''):
    """
    Look for things like stuck odometer, total fuel used, fuel level...
    """
    global LIST_OF_KPI_ERRORS
    kpi_pass = True

    engine_status = "off"
    previous_odometer = 0
    list_of_fuel_levels = []
    list_of_total_fuel_used = []
    list_of_engine_hours = []
    device_went_driving = 0
    device_esn = None
    device_power_unit_id = None

    with open(kpi_filename) as file_obj:
        reader_obj = csv.DictReader(file_obj)
        for row in reader_obj:
            if device_esn is None:
                device_esn = row["ESN"]
            if device_power_unit_id is None:
                device_power_unit_id = row["POWER_UNIT_ID"]
            if engine_status == "driving":
                device_went_driving += 1
                if row["ODOMETER"] == previous_odometer:
                    LIST_OF_KPI_ERRORS.append(
                            "KPI ERROR! Found stuck odometer! ESN: {},"\
                            "POWER UNIT ID: {}, CREATED_AT: {}".format(
                                device_esn,
                                device_power_unit_id,
                                row["CREATED_AT"]))
                    kpi_pass = False
                if row["FUEL_LEVEL"] not in list_of_fuel_levels:
                    list_of_fuel_levels.append(row["FUEL_LEVEL"])
                if row["TOTAL_FUEL_USED"] not in list_of_total_fuel_used:
                    list_of_total_fuel_used.append(row["TOTAL_FUEL_USED"])
                if row["ENGINE_HOURS"] not in list_of_engine_hours:
                    list_of_engine_hours.append(row["ENGINE_HOURS"])
                if row["SPEED"] == 0:
                    LIST_OF_KPI_ERRORS.append(
                            "KPI ERROR! Found zero speed value! ESN: {}, "\
                            "POWER UNIT ID: {}, CREATED_AT: {}".format(
                                device_esn, 
                                device_power_unit_id,
                                row["CREATED_AT"]))
            if row["ENGINE_STATUS"] == "off":
                engine_status = "off"
            elif row["ENGINE_STATUS"] == "idling":
                engine_status = "idling"
            elif row["ENGINE_STATUS"] == "driving":
                engine_status = "driving"
            previous_odometer = row["ODOMETER"]
            if (row["SHUTDOWN_REASON"] != "sigterm"
                    and row["SHUTDOWN_REASON"] != "null"):
                shutdown_reason = row["SHUTDOWN_REASON"]
                LIST_OF_KPI_ERRORS.append(
                    f"KPI ERROR! Suspicious Shutdown reason: '{shutdown_reason}', ESN: {device_esn}")
        if device_went_driving > 5:
            # We went driving for more than 5 HBs, typically 25 minutes
            if len(list_of_fuel_levels) < 3:
                LIST_OF_KPI_ERRORS.append(
                    f"KPI ERROR! Found stuck fuel level! ESN: {device_esn}")
            if len(list_of_total_fuel_used) < 3:
                LIST_OF_KPI_ERRORS.append(
                    f"KPI ERROR! Found stuck total fuel used! ESN: {device_esn}")
            if len(list_of_engine_hours) < 3:
                LIST_OF_KPI_ERRORS.append(
                    f"KPI ERROR! Found stuck engine hours! ESN: {device_esn}")

    print("# KPI Analysis on: {} ... KPI Pass? {}".format(
        kpi_filename, kpi_pass))


if __name__ == '__main__':
    print("Welcome to KPI Analysis!")
    setup()
    parse_arguments()

    if FILE_FOR_KPI_ANALYSIS != "":
        kpi_analysis(kpi_filename=FILE_FOR_KPI_ANALYSIS)
    else:
        for x in os.listdir():
            if (x.endswith(".csv")
                    and x.startswith("results_HB_")
                    and not x.startswith("results_HB_esn")):
                LIST_OF_FILES_FOR_KPI_ANALYSIS.append(x)
                kpi_analysis(kpi_filename=x)

    if len(LIST_OF_KPI_ERRORS) > 0:
        print("Found KPI errors! Saving to {}".format(KPI_ERRORS_FILENAME))
        with open(KPI_ERRORS_FILENAME, 'w') as f:
            for line in LIST_OF_KPI_ERRORS:
                f.write(f"{line}\n")
    else:
        print("Did not find any KPI failures!\n")
        if os.path.isfile(KPI_ERRORS_FILENAME):
            os.remove(KPI_ERRORS_FILENAME)

    teardown()
    sys.exit(0)

