# README #

Handle Snowflake Queries!

### What is this repository for? ###

* Using Python Snowflake Connector, do data analysis!

### How do I get set up? ###

* Install requirements
pip3 install -r requirements.txt
If you see an authentication for each query, try:
pip install "snowflake-connector-python[secure-local-storage]"

* Create a config/config.py with your credentials like below and update with credentials:  
config/config.py  
username ='username'
password ='password'
account = 'platformscience'
warehouse = 'QUERY_XSMALL'
database = 'PROD'

* Usage: \
python3 snowflake_query.py --help  # Shows options  

# Search all env
python3 snowflake_query.py --query_type=monitor_release --limit=10000 --start_date=2023-11-23 --end_date=2023-11-25 --filter_by_lmu_fw=V4.1g

# search for eng
python3 snowflake_query.py --query_type=monitor_release --limit=5000 --start_date=2023-11-23 --end_date=2023-11-25 --filter_by_lmu_fw=V4.1g --filter_by_environment=VVG

# search history of one device by esn
python3 snowflake_query.py --query_type=monitor_release --start_date=2023-11-19 --end_date=2023-11-25 --filter_by_esn=5571082771 --limit=5000

# search history by tablet serial
python3 snowflake_query.py --query_type=monitor_release --start_date=2023-7-1 --end_date=2023-7-3 --filter_by_tablet_serial=R52MB01HK6B --limit=5

# search list of esns
python3 snowflake_query.py --query_type=monitor_release --limit=5000 --file_of_esns=esn_files/12_1_2021_list.csv --start_date=2023-11-25 --end_date=2023-12-2

# search by tractor id
python3 snowflake_query.py --query_type=monitor_release --start_date=2023-11-1 --end_date=2023-11-20 --limit=50 --filter_by_tractor_id=12022

# convert list of Power IDs to ESNS
python3 snowflake_query.py --query_type=convert_power_ids_to_esn --start_date=2023-12-1 --end_date=2023-12-9 --file_of_power_unit_ids=esn_files/list_of_pids.csv --filter_by_environment=WMT --limit=5000

# Filter by Driver ID (User External ID)
python3 snowflake_query.py --query_type=monitor_release --start_date=2023-4-16 --end_date=2023-4-21 --filter_by_user_external_id=46771 --limit=5

# Get sherlock data
python3 snowflake_query.py --query_type=get_sherlock_data --filter_by_esn=5571102241
python3 snowflake_query.py --query_type=get_sherlock_data --file_of_esns=esn_files/5_25_2022_list.csv --limit=50

# Get HOS Logs
python3 snowflake_query.py --query_type=get_hos_logs --start_date=2023-5-16 --end_date=2023-5-19 --filter_by_tractor_id=211250 --limit=1000

# Search by user id
python3 snowflake_query.py --query_type=monitor_release --start_date=2023-5-30 --end_date=2023-5-31 --filter_by_user_id=1584617431500269 --limit=5

# Search Driver Performance Extracts
python3 snowflake_query.py --query_type=get_driver_performance --filter_by_environment=WMT --limit=500

# Search in DEV env
python3 snowflake_query.py --query_type=monitor_release --start_date=2023-6-14 --end_date=2023-6-16 --filter_by_esn=5572004945 --use_dev_env=yes --limit=500

# DORAN
python3 snowflake_query.py --query_type=get_doran_data --filter_by_esn=5572070847 --limit=500 --start_date=2023-10-25 --end_date=2023-11-1

# KPI analysis
python3 kpi_analysis_single_esn_history.py --file_for_kpi_analysis=results_HB_88041_5571089172.csv

# Run Snowflake and analysis on a filej
python3 execute_snowflake_and_kpi_analysis.py --file_of_esns=esn_files/FLTL-2056-monitor-shortened.csv

### Who do I talk to? ###
* Repo owner or admin
* Other community or team contact

### Contribution guidelines ###
Merge into develop.

