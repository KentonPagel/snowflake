#!/usr/bin/python3
"""
Snowflake Queries!
"""

import os
import sys
import pprint
import argparse
import traceback
from datetime import datetime, timedelta
import snowflake.connector as snow_conn
import pandas as pd
import json
import matplotlib.pyplot as plt
from pyvin import VIN
from vininfo import Vin
from time import sleep
import csv
import sys
import ast

from config import config

# Default values used for command line arguments
ELEMENT = ''
LIMIT = 10
START_DATE = (datetime.now()).strftime("%Y-%m-%d")
END_DATE = (datetime.now() + timedelta(days=1)).strftime("%Y-%m-%d")
FILTER_BY_ENVIRONMENT = ''
FILTER_BY_UPM = ''
FILTER_BY_JPOD_CONFIG = ''
FILTER_BY_JPOD_FW = ''
FILTER_BY_EE = ''
FILTER_BY_DEVICE_TYPE = ''
FILTER_BY_LMU_CFG = ''
FILTER_BY_LMU_FW = ''
FILTER_BY_ESN = ''
FILTER_BY_TABLET_SERIAL = ''
FILTER_BY_TRIP_ID = ''
FILTER_BY_TRACTOR_ID = ''
FILTER_BY_VIN = ''
FILTER_BY_USER_EXTERNAL_ID = ''
FILTER_BY_USER_ID = ''
FILTER_BY_DEVICE_ID = ''
FILTER_BY_CITY = ''
FILE_OF_ESNS = ''
FILE_OF_DRIVER_IDS = ''
FILE_OF_POWER_UNIT_IDS = ''
FILE_OF_DEVICE_IDS = ''
FILTER_BY_HAPY = ''
USE_DEV_ENV = ''
USE_INT_ENV = ''
USE_UAT_ENV = ''

# NOTE: Need to maintain as we grow our customers
ID_TO_ENV_DICT = {
        1004808615900915: "MVT",
        1240477565200877: "WERNER",
        1515373272600856: "CHARGER",
        1240141556800455: "FFI",
        573387697000233:  "VVG",
        1515289413900177: "WEI",
        1004808615900915: "SNI",
        1687301748400840: "USX",
        1841833636700461: "MAVERICK",
        1711355481900370: "STEVENS",
        1934521322200758: "CARGO",
        1896343646700359: "GYPSUM",
        1990604101100746: "PAPER",
        1930138038600284: "CRE",
        1401491084200378: "WMT",
        1153326205300111: "MVT",
        1825539577900772: "DG",
        1637200218100217: "EGL",
        1638116316400098: "KLLM",
        1428150949600199: "VERIHA",
        1512112476300748: "NAFTA",
        1557615440200272: "BAYLOR",
        1681383935500638: "FFE",
        1831515271200387: "KLLM-TY",
        1995754749100728: "PGT",
        1827180595300207: "CVL",
        2277429541300567: "NFI",
        1897386362500014: "FW",
        1903392483200123: "BISON",
        2352525692800528: "CRST",
        2374161559100732: "NRS",
        2392398728300017: "KAG",
        798254496700911: "SNI-UAT",
        2351860654700055: "MTN",
        2132428512400454: "COV",
        2436517345200616: "MNS",
        2334288603600477: "TET",
        2434856604500030: "CMA",
        2290381166300122: "AIM",
        2375008955800993: "TSI",
        637918746300072: "QA",
        999999999999999: "DEV"
}

# Tools in toolbelt:
# * count how many rows
#       SELECT COUNT(*) FROM "PROD"."ORION"."TELEMATICS_HEARTBEAT";
# * search for fuel level:
#       SELECT * from "PROD"."ORION"."TELEMATICS_HEARTBEAT" WHERE FUEL_LEVEL < 10 LIMIT 10;
# * SUM() - any use case here?
# 


RESULT_FILE_NAME = "results"

def save_results_to_file(response, filename_prefix='',
        sort_keyword='CREATED_AT'):
    number_of_responses = -1
    filename_to_save = RESULT_FILE_NAME
    if filename_prefix != '':
        filename_to_save = RESULT_FILE_NAME + "_" + filename_prefix
    selected_power_unit_id = FILTER_BY_TRACTOR_ID
    if selected_power_unit_id == '' and 'POWER_UNIT_ID' in response:
        try:
            first_power_unit_id = response['POWER_UNIT_ID'][0]
            second_power_unit_id = response['POWER_UNIT_ID'][1]
            if first_power_unit_id == second_power_unit_id:
                selected_power_unit_id = response['POWER_UNIT_ID'][0]
        except:
            pass
    selected_esn = FILTER_BY_ESN
    if selected_esn == '' and 'ESN' in response:
        try:
            first_esn = response['ESN'][0]
            second_esn = response['ESN'][1]
            if first_esn == second_esn:
                selected_esn = response['ESN'][0]
        except:
            pass
    selected_user_id = FILTER_BY_USER_ID
    selected_device_id = FILTER_BY_DEVICE_ID
    if selected_power_unit_id != '':
        filename_to_save += "_"
        filename_to_save += "{}".format(selected_power_unit_id)
    if selected_esn != '':
        filename_to_save += "_"
        filename_to_save += "{}".format(selected_esn)
    if selected_user_id != '':
        filename_to_save += "_"
        filename_to_save += "{}".format(selected_user_id)
    if selected_device_id != '':
        filename_to_save += "_"
        filename_to_save += "{}".format(selected_device_id)
    if FILE_OF_ESNS != '':
        # Don't create a new directory and strip ".csv"
        filename_to_save += "_{}".format(FILE_OF_ESNS.replace("/", "_")[:-4])
        number_of_responses = len(response)
    if FILE_OF_POWER_UNIT_IDS != '':
        # Don't create a new directory and strip ".csv"
        filename_to_save += "_{}".format(FILE_OF_POWER_UNIT_IDS.replace(
            "/", "_")[:-4])
        number_of_responses = len(response)
    if FILE_OF_DEVICE_IDS != '':
        # Don't create a new directory and strip ".csv"
        filename_to_save += "_{}".format(FILE_OF_DEVICE_IDS.replace(
            "/", "_")[:-4])
        number_of_responses = len(response)

    filename_to_save += ".csv"

    if sort_keyword and sort_keyword in response:
        # Assume user would like time sorted
        response[sort_keyword] = pd.to_datetime(response[sort_keyword])
        response = response.sort_values(by=sort_keyword, ascending=False)

    response.to_csv(filename_to_save, index=False)

    try:
        if len(response) == 0:
            print("\nNo response found!")
        elif filename_prefix == "sherlock":
            with pd.option_context('display.max_rows', 25,
                                   'display.max_columns', 15,
                                   'display.width', 200,
                                   'max_colwidth', 35
                                  ):
                print(response[['EVENT_TIMESTAMP_UTC',
                                'ESN',
                                'FAILSAFE',
                                'FAILSAFE_STATE',
                                'UBOOT_LINUXBOOT',
                                'SYSTEM_ERROR_LOG']])
        elif filename_prefix == "hos":
            with pd.option_context('display.max_rows', 25,
                                   'display.max_columns', 15,
                                   'display.width', 200,
                                   'max_colwidth', 35
                                  ):
                print(response[['CREATED_AT',
                                'POWER_UNIT_NUMBER',
                                'EVENT_DATE_UTC',
                                'MALFUNCTION_CODE',
                                'EVENT_TYPE',
                                'EVENT_CODE',
                                'Event Code Description']])
        elif filename_prefix == "dpm":
            with pd.option_context('display.max_rows', 25,
                                   'display.max_columns', 15,
                                   'display.width', 200,
                                   'max_colwidth', 30
                                  ):
                print(response[[
                                'CREATED_AT',
                                #'START_TIME',
                                #'END_TIME',
                                'ENV',
                                #'DEVICE_ID',
                                'DATA_EXTRACT.distance_drive',
                                #'DATA_EXTRACT.total_fuel_used',
                                #'DATA_EXTRACT.stop_idle_fuel_used',
                                #'DATA_EXTRACT.total_idle_fuel_used',
                                'DATA_EXTRACT.moving_time',
                                'DATA_EXTRACT.engine_time',
                                'DATA_EXTRACT.odometer_start',
                                #'DATA_EXTRACT.avg_engine_load',
                                #'DATA_EXTRACT.total_idle_time',
                                #'CALCULATED.MPG.distance_drive.total_fuel_used',
                                #'ASSET_ID',
                                #'USER_ID',
                               ]])
        else:
            with pd.option_context('display.max_rows', 25,
                                   'display.max_columns', 25,
                                   'display.width', 200,
                                   'max_colwidth', 55
                                  ):
                if 'CREATED_AT' in response:
                    desired_print_list = ['CREATED_AT',
                                    'ENV',
                                    'ESN',
                                    'EVENT',
                                    #'EVENT_DATA_NAME',
                                    'POWER_UNIT_ID',
                                    #'CVD_ID',
                                    #'EXTERNAL_ID',
                                    'IGNITION',
                                    'ENGINE_STATUS',
                                    'ODOMETER',
                                    #'TOTAL_FUEL_USED',
                                    #'FUEL_LEVEL',
                                    #'ENGINE_HOURS',
                                    #'SPEED',
                                    'LOCATION_DESCRIPTION',
                                    #'UPM',
                                    #'LMU FW',
                                    'EE Version',
                                    #'Go-Vehicle Version',
                                    'Go-Fast Version',
                                    #'EE Branch',
                                    'Jpod Cfg',
                                    'Jpod Fw',
                                    #'SHUTDOWN_REASON',
                                    #'Config Version',
                                    #'VIN',
                                    #'DOC.telematics.state_code',
                                    #'DOC.device_info.cvd_uptime',
                                    #'Lat',
                                    #'Lon',
                                    #'Country',
                                    #'State',
                                    #'City',
                                    #'all_validator_data.96.last_sa',
                                    #'DOC.telematics.gps.distance_diff',
                                    #'DOC.telematics.gps.total_distance',
                                    #'DOC.debug.engine_data_validator.multiple_sa',
                                    #'DOC.debug.engine_data_validator.selected_odometer_sa',
                                    #'DOC.debug.sa_finder_stats.last_ee_restart',
                                    #'last_ee_restart',
                                    ]
                    desired_print_list_filtered = list()
                    for item in desired_print_list:
                        if item in response:
                            desired_print_list_filtered.append(item)
                    print(response[desired_print_list_filtered])
                elif 'EVENT_TIMESTAMP_UTC' in response:
                    print(response[['EVENT_TIMESTAMP_UTC',
                                    'EVENT_TYPE',
                                    'CVD_SERIAL',
                                    #'ASSET_EXTERNAL_ID',
                                    #'TRAILER_EXTERNAL_ID',
                                    #'ASSET_ID',
                                    #'array_size(TIRES)',
                                    'TIRES[0]',
                                    'TIRES[1]',
                                    'TIRES[2]',
                                    'TIRES[3]',
                                    #'TIRES[4]',
                                    #'TIRES[5]',
                                    ]])
    except KeyError as e:
        sys.stdout.write("Failed to find correct keys...{}\n".format(e))

    sys.stdout.write("\nSaved results to {}\n".format(filename_to_save))
    if number_of_responses > 0:
        sys.stdout.write("Found    {} unique responses\n".format(
            number_of_responses))


# Move key columns to front
def move_column_around(df_doc="", string="", new_name=""):
    if string in df_doc:
        column = df_doc.pop(string)
        df_doc = pd.concat([column, df_doc], sort=False, axis=1)
        df_doc = df_doc.rename(columns=
            {string: new_name})
    return df_doc

def get_connection():
    """
    Set up connection and return it.
    :return:
    """
    connection = None
    try:
#       Snowflake usn, pwd (depracated)
#        connection = snow_conn.connect(
#            user=config.username,
#            password=config.password,
#            account=config.account,
#            warehouse=config.warehouse,
#            database=config.database)

        # SSO login
        connection = snow_conn.connect(
            user=config.username_sso,
            authenticator="externalbrowser",
            account=config.account,
            warehouse=config.warehouse,
            database=config.database)
    except snow_conn.errors.DatabaseError as error:
        sys.stdout.write("{}".format(error))
        sys.stdout.write("\nConnection ERROR! May need to connect to VPN\n")
        sys.exit(-1)
    return connection


def execute_query(connection=None, query=None, fetch='fetchall',
                  pandas=True, display_request=True, display_response=True):
    """
    Execute SQL query and return any response.
    Fully qualified table need to specify means
        "DB_NAME"."SCHEMA_NAME"."TABLE_NAME".
         Note values should be in double quotes, i.e:
        'SELECT * from "INT"."ORION"."TELEMATICS_GROUPS" LIMIT {};'.format(LIMIT)
    """
    response = ""
    start_time = datetime.now()
    if display_request:
        sys.stdout.write("{} Request  : {}".format(start_time, query))

    if pandas:
        # Use pandas to pretty print data response.
        response = pd.read_sql(query, connection)
        end_time = datetime.now()
        if display_response:
            sys.stdout.write("{} ({} seconds)\n\nQuery Response : \n{}\n".format(
                end_time, (end_time - start_time).seconds, response.to_string()))
    else:
        cursor = connection.cursor()
        try:
            cursor_obj = cursor.execute(query)
            if fetch:
                if fetch == "fetchall":
                    response = cursor_obj.fetchall()
                elif fetch == "fetchmany":
                    response = cursor_obj.fetchmany()
                else:
                    response = cursor_obj.fetchone()
            else:
                response = cursor_obj.fetchone()
        except Exception as e:
            sys.stdout.write("\tEXCEPTION! - {}".format(e))
            sys.stdout.write("\tTried to use SQL Query: {} ".format(query))
            traceback.print_exception()
            cursor.close()
            sys.exit(-1)
        finally:
            cursor.close()
        end_time = datetime.now()
        if display_response:
            sys.stdout.write("{} ({} seconds)\n\nQuery Response : {}\n".format(
                end_time, (end_time - start_time).seconds, response))
    return response


def plot_data(df=None, title_name=""):
    """
    X-axis can be a string, Y-axis values must be numeric.
    Scaling may be "off", i.e, odometer is large values vs on/off
    (boolean values).
    """
    try:
        # Plot single y-axis
        PLOT_SINGLE = False
        if PLOT_SINGLE:
            df.plot.line(title=title_name,
                    x='CREATED_AT', 
                    y=['ODOMETER'],
                    figsize=(10,6))  # Size in inches
            plt.xlabel('CREATED_AT')  
            plt.ylabel('Odometer')
            plt.grid(True)          # Enable grid for better readability
            plt.show()

        PLOT_MULTIPLE = True
        if PLOT_MULTIPLE:
            fig, ax1 = plt.subplots(figsize=(12, 8))

            ax1.set_xlabel('Sequence')
            ax1.set_ylabel('Odometer', color='tab:blue')
            ax1.plot(df['SEQUENCE_#'], df['ODOMETER'],
                    color='tab:blue', label='Odometer')
            ax1.tick_params(axis='y', labelcolor='tab:blue')

            ax2 = ax1.twinx()
            ax2.set_ylabel('Total Fuel Used', color='tab:red')
            ax2.plot(df['SEQUENCE_#'], df['TOTAL_FUEL_USED'],
                    color='tab:red', label='Total Fuel Used')
            ax2.tick_params(axis='y', labelcolor='tab:red')

            # Adding a title and grid for clarity
            plt.title('Odometer and Total Fuel Used vs. Sequence #')
            fig.tight_layout()  # Adjust layout to prevent overlap

            # Show the plot
            plt.show()

    except TypeError as type_exception:
        sys.stdout.write("Exception! - {}".format(type_exception))
        traceback.print_exception()
        sys.exit(-1)

def convert_deployment_id_to_env(id=0):
    env = "???"
    if id in ID_TO_ENV_DICT.keys():
       env = ID_TO_ENV_DICT[id]
    return env

def only_dict(d):
    '''
    Convert json string representation of dictionary to a python dict
    '''
    d = d.replace("null", '"null"')
    d = d.replace("null,\n", '"null"')
    d = d.replace("true", "True")
    d = d.replace("false", "False")

    return ast.literal_eval(d)
 
def get_list_of_formatted_data(df=None):
    """
    Retrieve debug info from nested JSON object.
    :param data_frame_object:
    :return:
    """
    FORMAT_DATA = True
    if not FORMAT_DATA:
        return df
    sys.stdout.write("\n{} Now converting data...\n".format(datetime.now()))

    if len(df) == 0:
        sys.stdout.write("\nNO DATA!\n")
        sys.exit(-1)

    if "DOC" in df:
        df_doc = pd.json_normalize(df["DOC"].apply(only_dict).\
            tolist()).add_prefix("DOC.")

    # Don't add: TRIP_ID, LOGGED_AT, DEPLOYMENT_ID
    if "CREATED_AT" in df and "DEPLOYMENT_ID" in df:
        df_doc = df[['CREATED_AT', 'LOGGED_AT', 'DEPLOYMENT_ID']].join([df_doc])
    else:
        df_doc = df

    single_device_history = False

    if (FILE_OF_ESNS != ''
            or FILE_OF_DRIVER_IDS != ''
            or FILE_OF_POWER_UNIT_IDS != ''
            or FILE_OF_DEVICE_IDS != ''
        or (FILTER_BY_ESN == ''
            and FILTER_BY_USER_EXTERNAL_ID == ''
            and FILTER_BY_TRACTOR_ID == ''
            and FILTER_BY_VIN == ''
            and FILTER_BY_DEVICE_ID == ''
            and FILTER_BY_USER_ID == '')
        ):
        # Filter out unique hits
        if single_device_history:
            df_doc = df_doc.sort_values(by="CREATED_AT", ascending=False)

            if 'DOC.entities.cvd.esn' in df_doc:
                sys.stdout.write("{} Dropping duplicates...".format(
                    datetime.now()))
                df_doc = df_doc.drop_duplicates(subset=['DOC.entities.cvd.esn'])
            if 'ESN' in df_doc:
                sys.stdout.write("{} Dropping duplicates ESN(s)!\n".format(
                    datetime.now()))
                df_doc = df_doc.drop_duplicates(subset=['ESN'])

    sys.stdout.write("\n{} Now load to json_struct...\n\n".format(datetime.now()))
    json_struct = json.loads(df.to_json(orient="records"))

    def convert_vin(vin="?", return_item="make"):
        # Compute make, model from VIN:
        make = "?"
        model = "?"
        year = "?"
        engine_manufacturer = "?"
        engine_model = "?"

        try:
            sleep(0.1)  # This gets HTTP 403 Forbidden errors sometimes
            vehicle = VIN(vin)
            if vehicle:
                make = vehicle.Make
                model = vehicle.Model
                year = vehicle.ModelYear
                engine_manufacturer = vehicle.EngineManufacturer
                engine_model = vehicle.EngineModel

        except Exception as e:
            # Backup plan in case of 403 errors
            if vin != "?" and not pd.isna(vin):
                sleep(0.1)
                vehicle = Vin(vin)
                make = vehicle.brand.manufacturer
                if make == 'Chrysler Mexico':
                    make = 'FREIGHTLINER'
                if len(vehicle.years) > 0:
                    year = str(vehicle.years[0])
        if return_item == "make":
            return make
        elif return_item == "model":
            return model
        elif return_item == "year":
            return year
        elif return_item == "engine_manufacturer":
            return engine_manufacturer
        elif return_item == "engine_model":
            return engine_model

    decode_vin = False 

    if (decode_vin
            and FILE_OF_ESNS == ''
            and FILE_OF_DRIVER_IDS == ''
            and FILE_OF_POWER_UNIT_IDS == ''
            and FILE_OF_DEVICE_IDS == ''
            and (FILTER_BY_ESN != ''
                or FILTER_BY_TRACTOR_ID != '')):
        # We have a single, ESN, decode the VIN for the entire column.
        doc_json_struct = json.loads(json_struct[0]['DOC'])
        make = convert_vin(
            doc_json_struct['entities']['power_unit']['vin'],
            return_item="make")
        model = convert_vin(
            doc_json_struct['entities']['power_unit']['vin'],
            return_item="model")
        year = convert_vin(
            doc_json_struct['entities']['power_unit']['vin'],
            return_item="year")
        engine_manufacturer = convert_vin(
            doc_json_struct['entities']['power_unit']['vin'],
            return_item="engine_manufacturer")
        engine_model = convert_vin(
            doc_json_struct['entities']['power_unit']['vin'],
            return_item="engine_model")

        df_doc['DOC.Computed.Truck.Make'] = make
        df_doc['DOC.Computed.Truck.Model'] = model
        df_doc['DOC.Computed.Truck.Year'] = year
        df_doc['DOC.Computed.Truck.EngineManufacturer'] = engine_manufacturer
        df_doc['DOC.Computed.Truck.EngineModel'] = engine_model
    elif decode_vin:
        # Decode VIN for each row, we have different ESNs.
        sys.stdout.write("{} Now converting VINs...\n".format(datetime.now()))
        df_doc['DOC.Computed.Truck.Make'] = df_doc.apply(
                lambda row: convert_vin(
                    row['DOC.entities.power_unit.vin'],
                    return_item="make"), axis=1)
        df_doc['DOC.Computed.Truck.Model'] = df_doc.apply(
                lambda row: convert_vin(
                    row['DOC.entities.power_unit.vin'],
                    return_item="model"), axis=1)
        df_doc['DOC.Computed.Truck.Year'] = df_doc.apply(
                lambda row: convert_vin(row['DOC.entities.power_unit.vin'],
                    return_item="year"), axis=1)
        df_doc['DOC.Computed.Truck.EngineManufacturer'] = df_doc.apply(
                lambda row: convert_vin(row['DOC.entities.power_unit.vin'],
                    return_item="engine_manufacturer"), axis=1)
        df_doc['DOC.Computed.Truck.EngineModel'] = df_doc.apply(
                lambda row: convert_vin(row['DOC.entities.power_unit.vin'],
                    return_item="engine_model"), axis=1)


    # Convert deployment ID to something human readable:
    if 'DEPLOYMENT_ID' in df_doc:
        df_doc['DOC.Computed.Deployment.HumanCode'] = df_doc.apply(
                lambda row: convert_deployment_id_to_env(
                    row['DEPLOYMENT_ID']), axis=1)

    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.debug.config_version",
            new_name="Config Version")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.debug.sa_finder_stats.last_ee_restart",
            new_name="last_ee_restart")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.debug.engine_data_validator.selected_odometer_spn",
            new_name="selected_odometer_spn")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.debug.engine_data_validator.selected_odometer_sa",
            new_name="selected_odometer_sa")

    #df_doc = move_column_around(df_doc=df_doc,
    #        string="DOC.device_info.cvd_sw.jpod_config.version",
    #        new_name="Jpod Cfg")
    #df_doc = move_column_around(df_doc=df_doc,
    #        string="DOC.device_info.cvd_sw.jpod_firmware.version",
    #        new_name="Jpod Fw")
    #df_doc = move_column_around(df_doc=df_doc,
    #        string="DOC.device_info.cvd_sw.lmu_firmware.version",
    #        new_name="LMU FW")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.device_info.cvd_sw.engine_events_ipk.version",
            new_name="EE Version")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.device_info.cvd_sw.go_vehicle.version",
            new_name="Go-Vehicle Version")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.device_info.cvd_sw.go_fast.version",
            new_name="Go-Fast Version")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.device_info.cvd_sw.upm_version.version",
            new_name="UPM")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.debug.tablet_messages",
            new_name="TABLET_MESSAGES")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.debug.engine_data_validator.multiple_sa",
            new_name="DOC.debug.engine_data_validator.multiple_sa")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.entities.user.id",
            new_name="USER_ID")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.entities.cvd.id",
            new_name="CVD_ID")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.telematics.location_description",
            new_name="LOCATION_DESCRIPTION")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.debug.shutdown_reason",
            new_name="SHUTDOWN_REASON")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.message_sequence_number",
            new_name="SEQUENCE_#")
    #df_doc = move_column_around(df_doc=df_doc,
    #        string="DOC.telematics.ignition",
    #        new_name="IGNITION")
    #df_doc = move_column_around(df_doc=df_doc,
    #        string="DOC.telematics.rpm",
    #        new_name="RPM")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.telematics.speed",
            new_name="SPEED")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.telematics.fuel_level",
            new_name="FUEL_LEVEL")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.telematics.engine_hours",
            new_name="ENGINE_HOURS")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.telematics.total_fuel_used",
            new_name="TOTAL_FUEL_USED")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.telematics.odometer",
            new_name="ODOMETER")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.telematics.engine_status",
            new_name="ENGINE_STATUS")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.telematics.event",
            new_name="EVENT")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.telematics.event_data.name",
            new_name="EVENT_DATA_NAME")
    # Convert non-string column to string column
    #if 'DOC.entities.cvd.id' in df_doc:
    #    df_doc['DEVICE_ID'] = df_doc['DOC.entities.cvd.id'].astype(str)
    #df_doc = move_column_around(df_doc=df_doc,
    #        string="DOC.entities.user.external_id",
    #        new_name="EXTERNAL_ID")
    #df_doc = move_column_around(df_doc=df_doc,
    #        string="DOC.entities.user.id",
    #        new_name="USER_ID")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.entities.power_unit.vin",
            new_name="VIN")

    LOOK_FOR_FUEL_LEVEL_STUFF = False
    if LOOK_FOR_FUEL_LEVEL_STUFF:
        df_doc = move_column_around(df_doc=df_doc,
                string="DOC.debug.engine_data_validator.all_validator_data."\
                        "96.sa_values.33",
                new_name="DOC.debug.engine_data_validator.all_validator_data."\
                        "96.sa_values.33")
        df_doc = move_column_around(df_doc=df_doc,
                string="DOC.debug.engine_data_validator.all_validator_data."\
                        "96.sa_valid.33",
                new_name="DOC.debug.engine_data_validator.all_validator_data."\
                        "96.sa_valid.33")
        df_doc = move_column_around(df_doc=df_doc,
                string="DOC.debug.engine_data_validator.all_validator_data."\
                        "96.sa_diffs.33",
                new_name="DOC.debug.engine_data_validator.all_validator_data."\
                        "96.sa_diffs.33")
        df_doc = move_column_around(df_doc=df_doc,
                string="DOC.debug.engine_data_validator.all_validator_data."\
                        "96.last_sa",
                new_name="DOC.debug.engine_data_validator.all_validator_data."\
                        "96.last_sa")
        df_doc = move_column_around(df_doc=df_doc,
                string="DOC.telematics.fuel_level",
                new_name="DOC.telematics.fuel_level")

    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.entities.power_unit.power_unit_id",
            new_name="POWER_UNIT_ID")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.entities.cvd.esn",
            new_name="ESN")
    df_doc = move_column_around(df_doc=df_doc,
            string="DOC.Computed.Deployment.HumanCode",
            new_name="ENV")
    df_doc = move_column_around(df_doc=df_doc,
            string="CREATED_AT",
            new_name="CREATED_AT")

    # Just rename the column
    df_doc = df_doc.rename(
            columns={"DOC.device_info.cvd_sw.engine_events_ipk.branch":
                     "EE Branch"})
    df_doc = df_doc.rename(
            columns={"DOC.telematics.latitude": "Lat"})
    df_doc = df_doc.rename(
            columns={"DOC.telematics.longitude": "Lon"})
    df_doc = df_doc.rename(
            columns={"DOC.telematics.relative_position.city": "City"})
    df_doc = df_doc.rename(
            columns={"DOC.telematics.relative_position.state_code": "State"})
    df_doc = df_doc.rename(
            columns={"DOC.telematics.relative_position.country_code": "Country"})
#    df_doc = df_doc.rename(
#            columns={"DOC.entities.cvd.id": "CVD_ID"})

    # Delete unnecessary columns:
    def delete_from_column(string=""):
        if string in df_doc:
            del df_doc[string]
    delete_from_column("DOC.group_id")
    delete_from_column("DOC.session_id")
    delete_from_column("DOC.debug.bendix_data.get.2xx")
    delete_from_column("DOC.debug.bendix_data.get.4xx")
    delete_from_column("DOC.debug.bendix_data.get.5xx")
    delete_from_column("DOC.debug.bendix_data.get.unknown")
    delete_from_column("DOC.debug.bendix_data.post.2xx")
    delete_from_column("DOC.debug.bendix_data.post.4xx")
    delete_from_column("DOC.debug.bendix_data.post.5xx")
    delete_from_column("DOC.debug.bendix_data.post.unknown")
    delete_from_column("DOC.debug.bendix_data.status_bitfield")
    delete_from_column("DOC.debug.cellular_info.rsrp_rssi")
    delete_from_column("DOC.debug.cellular_info.rsrq")
    delete_from_column("DOC.debug.cellular_info.service")
    delete_from_column("DOC.debug.invalid_speed_spike_occurrences")
    #delete_from_column("DOC.debug.jbus_debug_data.bus_mode")
    #delete_from_column("DOC.debug.jbus_debug_data.can_rx_error_count")
    #delete_from_column("DOC.debug.jbus_debug_data.can_tx_error_count")
    #delete_from_column(
    #        "DOC.debug.jbus_debug_data.missing_jbus_state_machine")
    delete_from_column("DOC.debug.jbus_debug_data.source_addresses.247")
    delete_from_column("DOC.debug.jbus_debug_data.source_addresses.250")
    delete_from_column("DOC.debug.jbus_debug_data.source_addresses.5054")
    delete_from_column("DOC.debug.jbus_debug_data.source_addresses.917")
    delete_from_column("DOC.debug.jbus_debug_data.source_addresses")
    delete_from_column("DOC.debug.memory_wifi_stats.engine_events_mem")
    delete_from_column("DOC.debug.memory_wifi_stats.hsl_mem")
    delete_from_column(
            "DOC.debug.memory_wifi_stats.memory_ign_off_mitigation")
    delete_from_column("DOC.debug.memory_wifi_stats.memory_mitigation")
    delete_from_column("DOC.debug.memory_wifi_stats.socket_mitigation")
    delete_from_column("DOC.debug.total_fuel_used_drop")
    delete_from_column("DOC.debug.wifi.info")
    delete_from_column("DOC.debug.wifi.power_value")
    delete_from_column("DOC.device_info.comm_type")
    delete_from_column("DOC.device_info.rssi")
    delete_from_column("DOC.device_info.tablet_power_info.plugged")
    delete_from_column("DOC.device_info.tablet_power_info.scale")
    delete_from_column("DOC.device_info.tablet_power_info.technology")
    delete_from_column("DOC.device_info.tablet_power_info.temperature")
    delete_from_column("DOC.device_info.tablet_power_info.voltage")
    delete_from_column("DOC.device_info.wifi_ap_chan")

    WANT_TO_PLOT_STUFF = False
    if WANT_TO_PLOT_STUFF and single_device_history:
        plot_data(df_doc, 'Chart')

    return df_doc

def filter_for_certain_values():
    """
    Only get results with certain values
    """
    filtered_sql_cmd = ''

    # Search events: "periodic_update", "critical_event"
    SEARCH_FOR_ANY_CRITICAL_EVENT = False
    if SEARCH_FOR_ANY_CRITICAL_EVENT :
        interesting_event = "ignition_on"
        filtered_sql_cmd += "AND doc:telematics:event="\
                "'{}'\n".format(interesting_event)
        filtered_sql_cmd += "AND doc:device_info:cvd_sw:\"go-vehicle\":"\
                "version LIKE '3.7.22'\n"

    SEARCH_FOR_STARTUP = False
    if SEARCH_FOR_STARTUP:
        interesting_event = "startup"
        filtered_sql_cmd += "AND doc:telematics:event="\
                "'{}'\n".format(interesting_event)
        filtered_sql_cmd += "AND doc:device_info:cvd_sw:\"go-vehicle\":"\
                "version LIKE '3.7.9'\n"

    SEARCH_FOR_FOLLOW_TIME_VIOLATION = False
    if SEARCH_FOR_FOLLOW_TIME_VIOLATION:
        interesting_event = "follow_time_violation"
        filtered_sql_cmd += "AND doc:telematics:event_data:name="\
                "'{}'\n".format(interesting_event)

    # Search critical events: "collision_avoidance_haptic_warning"
    #                         "emergency_braking_active"
    #                         "roll_stability"
#    interesting_critical_event = "collision_avoidance_haptic_warning"
#    filtered_sql_cmd += "AND doc:telematics:event_data:name in \n"\
#            "(\n'roll_stability', "\
#            "\n''\n)\n"

    # Search EE version
    # "5.18.4-5", "5.20.2-172", "5.20.3-2", "5.19.11-168"
    #interesting_ee_version = "5.20.2-172"
    #filtered_sql_cmd += "AND doc:device_info:cvd_sw:engine_events_ipk:version="\
    #        "'{}'\n".format(interesting_ee_version)

    # Search specific VINs:
    #filtered_sql_cmd += "AND doc:entities:power_unit.vin LIKE '3AKJHHDR8RSUS4958'\n"

    # Specific jpod version
#    interesting_jpod_version = "77"  # "73", "77"
#    filtered_sql_cmd += "AND doc:device_info:cvd_sw:jpod_config:version="\
#            "'{}'\n".format(interesting_jpod_version)

    #interesting_sandman_version = ["1.1.0-16", "1.1.1-17"]
    #filtered_sql_cmd += "AND doc:device_info:cvd_sw:sandman:version in (\n"
    #for version in interesting_sandman_version[:-1]:
    #    filtered_sql_cmd += "'{}',\n".format(version)
    #filtered_sql_cmd += "'{}')\n".format(interesting_sandman_version[-1])  # Delete comma

    # Filter for 2024 
    SEARCH_FOR_VIN_2024 = False
    if SEARCH_FOR_VIN_2024:
        filtered_sql_cmd += "AND doc:entities:power_unit.vin LIKE '_________R_______'\n"

    # Search for specific VIN types
    # -- Check for late Paccar
    #filtered_sql_cmd += "AND doc:entities:power_unit.vin LIKE '1NK______M_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '1XK______M_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '2NK______M_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '2XK______M_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '3BK______M_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '3NK______M_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '3WK______M_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '1NP______M_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '1XP______M_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '2NP______M_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '2XP______M_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '3BP______M_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '3NM______M_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '1NK______N_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '1XK______N_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '2NK______N_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '2XK______N_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '3BK______N_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '3NK______N_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '3WK______N_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '1NP______N_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '1XP______N_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '2NP______N_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '2XP______N_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '3BP______N_______'\n"
    #filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE '3NM______N_______'\n"

    # Search for Peterbilts
    SEARCH_FOR_PETERBILTS_2024 = False
    if SEARCH_FOR_PETERBILTS_2024:
        filtered_sql_cmd += "AND (doc:entities:power_unit.vin LIKE "\
                "'1NP______R_______'\n"
        filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE "\
                "'1XP______R_______'\n"
        filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE "\
                "'2NP______R_______'\n"
        filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE "\
                "'2XP______R_______'\n"
        filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE "\
                "'3BP______R_______'\n"
        filtered_sql_cmd += "OR doc:entities:power_unit.vin LIKE "\
                "'3NM______R_______')\n"

    # Now magic happens:
#    filtered_sql_cmd += "AND doc:debug:tablet_messages LIKE '%socket_failure%'\n"

    # Engine Status
    #filtered_sql_cmd += "AND doc:telematics:engine_status = 'off'\n"
    

    #filtered_sql_cmd += "AND doc:telematics:engine_status = 'driving'\n"
    #filtered_sql_cmd += "AND doc:debug:engine_data_validator.multiple_sa LIKE '%\"odometer HR, spn:917\": {\"39%'\n"
    #filtered_sql_cmd += "AND doc:entities:power_unit.vin LIKE '_________________'\n"

    # FLTL-1161: Search for Odometer SA=49, VIN year is 'M', 2021
#    filtered_sql_cmd += "AND doc:telematics:engine_status = 'driving'\n"
    #filtered_sql_cmd += "AND doc:debug:engine_data_validator.multiple_sa LIKE '%\"odometer HR, spn:917\": {\"49%'\n"
#    filtered_sql_cmd += "AND doc:entities:power_unit.vin LIKE '_________M_______'\n"

    # FLTL-1161: Search for Odometer SA=39, VIN year is 'N', 2022
#    filtered_sql_cmd += "AND doc:telematics:engine_status = 'driving'\n"
#    filtered_sql_cmd += "AND doc:debug:engine_data_validator.multiple_sa LIKE '%\"odometer HR, spn:917\": {\"39%'\n"
#    filtered_sql_cmd += "AND doc:entities:power_unit.vin LIKE '_________N_______'\n"

    # -------------------------- Search switched_battery_voltage ------------:
    #filtered_sql_cmd += "AND doc:telematics:switched_battery_voltage = 0\n"
    #filtered_sql_cmd += "AND doc:telematics:engine_status = 'driving'\n"
    #filtered_sql_cmd += "AND doc:telematics:engine_status = 'off'\n"

    # -------------------------- Filter by ignition value ---------------------:
    # filtered_sql_cmd += "AND doc:telematics:ignition = True\n"

    # -------------------------- Filter for jumps ----------------------------:
    #filtered_sql_cmd += "AND doc:telematics:odometer_jump != 0\n"
    #OR doc:telematics:engine_hours_jump != 0\n\
    #OR doc:telematics:gps_position_jump != 0\n"

    # -------------------------- Filter for stuck odometer -------------------:
    SEARCH_FOR_STUCK_ODOMETER_OR_FUEL = False
    if SEARCH_FOR_STUCK_ODOMETER_OR_FUEL:
        # Search for both stuck odometer and fuel
        # Options:
        #   'vbusStuck'
        #   'fuelStuck'
        filtered_sql_cmd += "AND LENGTH(doc:debug:stuck_vbus_occurrences)>7\n"
        filtered_sql_cmd += 'AND doc:telematics:engine_status = \'driving\'\n'

    FILTER_LOCATION = False
    if FILTER_LOCATION:
        LIST_OF_LOCATIONS = [
                             #'Stillwater, MN',
                             'Green Bay, WI',
                             'Lyndon Station, WI',
                             #'New Richmond, WI',
                             ]
        filtered_sql_cmd += "AND (\n"
        for i, location_of_interest in enumerate(LIST_OF_LOCATIONS):
            filtered_sql_cmd += "\tdoc:telematics:location_description"\
                " like '%{}%'".format(location_of_interest)
            if i < len(LIST_OF_LOCATIONS) - 1:
                filtered_sql_cmd += "\tOR\n"
        filtered_sql_cmd += "\n)\n"

    # ------------ Look for odometer = 0 w/ignition off ----------------------:
    #filtered_sql_cmd += 'AND doc:telematics:odometer = 0\n'
    #filtered_sql_cmd += 'AND doc:telematics:engine_status != \'off\'\n'
    #filtered_sql_cmd += 'AND doc:telematics:engine_status = \'driving\'\n'
    
    # --------------------- Search for various failures ----------------------:
    #

    # --------------------- Search for SA=0 and jbus failures----------------------:
#    filtered_sql_cmd += 'AND doc:debug:sensor_failures:jbus.cause ='\
#        '\'the odometer could not be read for too many consecutive times\'\n'
#    filtered_sql_cmd += "AND doc:debug:engine_data_validator.multiple_sa LIKE '%\"odometer HR, spn:917\": {\"0%'\n"

#    filtered_sql_cmd += "AND doc:debug:config_version != 'cvd-cvd-sa-safe-mode-v3'\n"

    # --------------------- Search for rolled over values----------------------:
    #filtered_sql_cmd += "AND doc:debug:engine_data_validator.all_validator_data"\
    #        " LIKE '%sa_rolled_over_values%'\n"
    
    SEARCH_JPOD_ERRORS = False
    if SEARCH_JPOD_ERRORS:
        # TODO make smarter to find location changing:
        #   doc:telematics:location_description
        filtered_sql_cmd += "AND doc:telematics:event != 'startup'\n"
        filtered_sql_cmd += "AND doc:telematics:event != 'ignition_on'\n"
        filtered_sql_cmd += "AND doc:telematics:engine_status = 'driving'\n"
        filtered_sql_cmd += "AND "\
            "doc:debug:engine_data_validator.multiple_sa LIKE "\
            "'%\"odometer HR, spn:917\": {}, \"odometer LR, spn:245\": {}%'\n"
        filtered_sql_cmd += "AND doc:debug:sa_finder_stats:last_ee_restart ="\
            "'0001-01-01T00:00:00Z'\n"


    # --------------------- Search for speed issues ----------------------:
    SEARCH_SPEED_SENSOR_FAILURES = False
    if SEARCH_SPEED_SENSOR_FAILURES:
        filtered_sql_cmd += "AND doc:debug:sensor_failures:speed.cause = "\
            "'GPS says truck is in motion and the engine is ON, but wheel "\
            "speed value read is 0'\n"

    FILTER_SPECIFIC_EE_BRANCH = False
    if FILTER_SPECIFIC_EE_BRANCH:
        interesting_ee_branch_list = [
            "hsl_master",
            "release/5.21.5-Paccar"
            ]
        filtered_sql_cmd += "AND doc:device_info:cvd_sw:engine_events_ipk:"\
                "branch in (\n"
        for version in interesting_ee_branch_list[:-1]:
            filtered_sql_cmd += "\t'{}',\n".format(version)
        filtered_sql_cmd += "\t'{}')\n".format(interesting_ee_branch_list[-1])

    FILTER_SPECIFIC_EE_VERSION = False
    if FILTER_SPECIFIC_EE_VERSION:
        filtered_sql_cmd += "AND doc:device_info:cvd_sw:engine_events_ipk:"\
                "version LIKE '5.2%'\n"
        #interesting_ee_list = [
        #        "5.19.14-6",
        #        "5.21.5-1",
        #        "",
        #        ]
        #filtered_sql_cmd += "AND doc:device_info:cvd_sw:engine_events_ipk:"\
        #        "version in (\n"
        #for version in interesting_ee_list[:-1]:
        #    filtered_sql_cmd += "\t'{}',\n".format(version)
        #filtered_sql_cmd += "\t'{}')\n".format(interesting_ee_list[-1])

    FILTER_SPECIFIC_JPOD_FW = False
    if FILTER_SPECIFIC_JPOD_FW:
        filtered_sql_cmd += "AND doc:device_info:cvd_sw:jpod_firmware:"\
                "version = '14c'\n"

    FILTER_ODOMETER_SELECTED_SA = False
    if FILTER_ODOMETER_SELECTED_SA:
        filtered_sql_cmd += "AND doc:debug:engine_data_validator:"\
                "selected_odometer_sa = 23\n"

    FILTER_SPECIFIC_GO_VEHICLE_FAST_VERSION = True
    if FILTER_SPECIFIC_GO_VEHICLE_FAST_VERSION:
        filtered_sql_cmd += "AND doc:device_info:cvd_sw:\"go_fast\":"\
                "version LIKE '%2024%'\n"
        filtered_sql_cmd += "AND doc:telematics:event in (\n"
        interesting_events = [
           "shutdown",
           "startup",
        ]
        for version in interesting_events[:-1]:
            filtered_sql_cmd += "\t'{}',\n".format(version)
        filtered_sql_cmd += "\t'{}')\n".format(interesting_events[-1])

        #filtered_sql_cmd += "AND LENGTH(doc:device_info:cvd_sw:"\
        #        "engine_events_ipk:version) IS NULL\n"
        #filtered_sql_cmd += "AND doc:telematics:event = 'shutdown'\n"
        #filtered_sql_cmd += "AND doc:telematics:event = 'startup'\n"
        #filtered_sql_cmd += "AND doc:device_info:device_type != 'ctp2'\n"


    FILTER_SPECIFIC_GO_VEHICLE_VERSION = False
    if FILTER_SPECIFIC_GO_VEHICLE_VERSION:
        # DOC.device_info.cvd_sw.go-vehicle.version
        #filtered_sql_cmd += "AND doc:device_info:cvd_sw:\"go-vehicle\":"\
        #        "version LIKE '3.7.22%'\n"
        #filtered_sql_cmd += "AND doc:device_info:cvd_sw:\"go-vehicle\":"\
        #        "version LIKE '3.7.%'\n"

        filtered_sql_cmd += "AND LENGTH(doc:device_info:cvd_sw:"\
                "engine_events_ipk:version) IS NULL\n"
        #filtered_sql_cmd += "AND LENGTH(doc:entities:power_unit:vin) IS NULL\n"
        filtered_sql_cmd += "AND doc:telematics:event != 'startup'\n"
        #filtered_sql_cmd += "AND doc:telematics:event = 'driving'\n"
        filtered_sql_cmd += "AND doc:device_info:device_type != 'ctp2'\n"
        #filtered_sql_cmd += "AND doc:device_info:cvd_uptime > 86400*5\n"

    FILTER_SPECIFIC_JPOD_VERSIONS = False
    if FILTER_SPECIFIC_JPOD_VERSIONS:
        filtered_sql_cmd += "AND doc:device_info:cvd_sw:jpod_config:"\
                "version not like '1'\n"

    FILTER_SPECIFIC_UPM_VERSIONS = False
    if FILTER_SPECIFIC_UPM_VERSIONS:
        interesting_upm_list = [
                "3.52.255-188",
                "3.52.256-189",
                "3.52.257-190",
                "3.52.258-191",
                "3.52.259-192",
                "3.52.260-193",
                "3.52.261-194",
                "3.52.262-195",
                "3.52.263-196",
                "3.52.264-197",
                "3.52.265-198",
                "3.52.266-199",
                "",
                ]
        filtered_sql_cmd += "AND doc:device_info:cvd_sw:upm_version:"\
                "version in (\n"
        for version in interesting_upm_list[:-1]:
            filtered_sql_cmd += "\t'{}',\n".format(version)
        filtered_sql_cmd += "\t'{}')\n".format(interesting_upm_list[-1])

    FILTER_SPECIFIC_SAFINDER_VERSION = False
    if FILTER_SPECIFIC_SAFINDER_VERSION:
        filtered_sql_cmd += "AND doc:device_info:cvd_sw:safinder:"\
                "version LIKE '1.3.3%'\n"

    FILTER_FUEL_LEVEL = False
    if FILTER_FUEL_LEVEL:
        filtered_sql_cmd += 'AND doc:telematics:fuel_level > 105\n'
        filtered_sql_cmd += 'AND doc:telematics:fuel_level < 1000\n'
        #filtered_sql_cmd += "AND doc:telematics:engine_status = 'driving'\n"
        #filtered_sql_cmd += "AND doc:device_info:cvd_sw:engine_events_ipk:"\
        #        "version LIKE '5.21.4-%'\n"

    FILTER_ODOMETER_JUMP = False
    if FILTER_ODOMETER_JUMP:
        filtered_sql_cmd += 'AND doc:telematics:odometer_jump != 0\n'

    # -------------------------- Search total_fuel_used ------------:
    #filtered_sql_cmd += "AND doc:telematics:total_fuel_used != 16202.5\n"
    #filtered_sql_cmd += "AND doc:telematics:total_fuel_used != 37134.1\n"

    # -------------------------- Search 4.1k increased reboots ------------:
    FILTER_SPECIFIC_FW_VERSIONS = False
    if FILTER_SPECIFIC_FW_VERSIONS:
        interesting_fw_list = [
                "V4.1i",
                #"V4.1k",
                ]
        filtered_sql_cmd += "AND doc:device_info:cvd_sw:lmu_firmware:"\
                "version in (\n"
        for version in interesting_fw_list[:-1]:
            filtered_sql_cmd += "\t'{}',\n".format(version)
        filtered_sql_cmd += "\t'{}')\n".format(interesting_fw_list[-1])

    FILTER_FALSE_IGNITION_WHILE_MOVING = False
    if FILTER_FALSE_IGNITION_WHILE_MOVING:
        filtered_sql_cmd += "AND doc:telematics:event != 'startup'\n"
        filtered_sql_cmd += "AND doc:telematics:event != 'shutdown'\n"
        filtered_sql_cmd += "AND doc:telematics:event != 'ignition_off'\n"
        filtered_sql_cmd += "AND doc:telematics:event != 'ignition_on'\n"
        filtered_sql_cmd += "AND doc:telematics:ignition = 'false'\n"
        filtered_sql_cmd += "AND doc:telematics:engine_status = 'driving'\n"
        filtered_sql_cmd += "AND doc:telematics:speed > 10\n"

    FILTER_SPECIFIC_SHUTDOWN_REASON = False
    if FILTER_SPECIFIC_SHUTDOWN_REASON:
        filtered_sql_cmd += "AND doc:debug:shutdown_reason != ''\n"
        #interesting_shutdown_reason_list = [
            #'unexpected CVD reboot',
            #'periodic reboot',
            #'cell issue EE restart',
            #'exception no reason found.',
            #'sigterm',
            #'one shot sleep timer called',
            #'rebooting cvd due to stuck jpod or fuel issues',
            #''
            #]
        #filtered_sql_cmd += "AND doc:debug:shutdown_reason"\
        #        " in (\n"
        #for version in interesting_shutdown_reason_list[:-1]:
        #    filtered_sql_cmd += "\t'{}',\n".format(version)
        #filtered_sql_cmd += "\t'{}')\n".format(
        #        interesting_shutdown_reason_list[-1])

    FILTER_SHERLOCK_LMU_MONITOR_CRASHED = False
    if FILTER_SHERLOCK_LMU_MONITOR_CRASHED: 
        filtered_sql_cmd += "AND SYSTEM_ERROR_LOG LIKE "\
                "'%calamp_lmu_monitor: cmm has terminated%'\n"

    return filtered_sql_cmd

def filter_for_certain_values_go_fast():
    """
    Additional SQL filters
    """
    filtered_sql_cmd = ''

    SEARCH_FOR_SPECIFIC_FILES = True
    if SEARCH_FOR_SPECIFIC_FILES:
        interesting_filepath_list = [
            '/var/log/pltsci/gohal/go-fast-stdout.log',
            '/var/log/pltsci/gohal/gps_updater.log',
            '/var/log/pltsci/gohal/integration_tests.log',
            '/var/log/pltsci/gohal/io_manager.log',
            '/var/log/pltsci/gohal/ipc.log',
            '/var/log/pltsci/gohal/mid.log',
            '/var/log/pltsci/gohal/periodic.log',
            '/var/log/pltsci/gohal/time_sync.log',
            '/var/log/pltsci/gohal/wifi_monitor.log',
            '/var/log/pltsci/gohal/lmu_app.log',
            '/var/log/pltsci/gohal/sleep_manager.log',
            '/var/log/pltsci/gohal/gv.log',
            '/var/log/pltsci/gohal/cvd_actions.log',
            ]
        filtered_sql_cmd += 'AND filepath in (\n'
        for funFile in interesting_filepath_list[:-1]:
            filtered_sql_cmd += "\t'{}',\n".format(funFile)
        filtered_sql_cmd += "\t'{}'\n)\n".format(interesting_filepath_list[-1])

    return filtered_sql_cmd

def monitor_release():
    """
    Given an ESN list and several parameters, display import version information.
    Exits with failure state if mac address is default or not all ESNs
    reported in via heartbeat with given filters.
    Elements: ID, DEPLOYMENT_ID, TELMATICS_GROUP_ID, EVENT, FUEL_LEVEL, GEO,
              LOCATION_DESCRIPTION, CITY, STATE_CODE, COUNTRY_CODE, ODOMETER,
              ODOMETER_GROUP, ODOMETER_JUMP, IGNITION, RPM, ENGINE_HOURS,
              ENGINE_HOURS_JUMP, WHEELS_IN_MOTION, SPEED, HEADING, ACCURACY,
              SATELLITES, HDOP, GPS_VALID SOURCE, STATE_CODE_ABSOLUTE,
              TOTAL_FUEL_USED, TRIP_ID, HAS_IDLE_PERIOD, ASSET_ID, USER_ID,
              TABLET_ID, LOGGED_AT, CREATED_AT, UPDATED_AT, UUID

            logged_at = time heartbeat created on CVD
            created_at = time we receive the heartbeat

    Can be called with the following arguments:
       Search by dates      : --start=2021-10-01 --end=2021-10-11
       Search by environment: --filter_by_environment=WMT
       Search by jpod cfg   : --filter_by_jpod_config=56
       Search by jpod fw    : --filter_by_jpod_fw=12q
       Search by UPM        : --filter_by_upm=3.22.25-286
       Search by EE         : --filter_by_ee=5.12.0-147
       Search by type       : --filter_by_device_type=LMU5541
       Search by LMU CFG    : --filter_by_lmu_cfg=55.50
       Search by LMU FW     : --filter_by_lmu_fw=V4.1d
       Search by ESN        : --filter_by_esn=5572004945
       Search by Trip ID    : --filter_by_trip_id=26424
       Search by HaPy       : --filter_by_hapy=1.1.7-16
       Search list of ESNS  : --file_of_esns=41F_Beta_VVG.csv
       Use DEV environement : --use_dev_env=yes
       Use INT environement : --use_int_env=yes
       Use UAT environement : --use_uat_env=yes
    """
    def print_table(rows):
        max_widths = []
        for column in zip(*rows):
            max_widths.append(max([len(text) for text in column]))
        template = '  '.join(['{{:<{}}}'.format(width) for width in max_widths])
        return '\n'.join([template.format(*row) for row in rows])

    list_of_requested_esns = []
    list_of_requested_power_units = []
    list_of_heartbeated_esns = []
    list_of_requested_device_ids = []

    start_time = datetime.now()

    SEARCH_LAST_HOUR = False
    CURRENT_DATE = (datetime.now()).strftime("%Y-%m-%d").replace('-0', '-')
    global START_DATE
    START_DATE = str(START_DATE).replace('-0', '-')
    if CURRENT_DATE == START_DATE:
        SEARCH_LAST_HOUR = True


    sql_cmd = '\nSELECT \n'
    sql_cmd += 'doc, \n'
    sql_cmd += 'trip_id, \n'
    sql_cmd += 'city, \n'
    sql_cmd += 'created_at, \n'
    sql_cmd += 'logged_at, \n'
    sql_cmd += 'deployment_id\n'
    # TELEMATICS_HEARTBEAT updates every 15 minutes
    if USE_DEV_ENV == 'yes':
        sql_cmd += ' from "DEV"."ORION"."TELEMATICS_HEARTBEAT" \n'
    elif USE_INT_ENV == 'yes':
        sql_cmd += ' from "INT"."ORION"."TELEMATICS_HEARTBEAT" \n'
    elif USE_UAT_ENV == 'yes':
        sql_cmd += ' from "UAT"."ORION"."TELEMATICS_HEARTBEAT" \n'
    else:
        sql_cmd += ' from "PROD"."ORION"."TELEMATICS_HEARTBEAT" \n'
    if START_DATE and END_DATE and not SEARCH_LAST_HOUR:
        sql_cmd += 'WHERE created_at BETWEEN \'{}\' and \'{}\' \n'.format(
                START_DATE, END_DATE)
    else:
        sql_cmd += "WHERE \n"
    if SEARCH_LAST_HOUR:
        #sql_cmd += 'CREATED_AT > current_timestamp - INTERVAL \'1 hour\'\n'
        sql_cmd += 'CREATED_AT > current_timestamp - INTERVAL \'35 minutes\'\n'
    if FILTER_BY_ENVIRONMENT != '':
        if START_DATE and END_DATE:
            sql_cmd += 'AND \n'
        else:
            sql_cmd += 'WHERE \n'
        sql_cmd += 'utils.deployment_id_to_customer_name(deployment_id) '\
                   '= \'{}\' \n'.format(FILTER_BY_ENVIRONMENT)
    if FILE_OF_ESNS != '':
        try:
            with open(file=FILE_OF_ESNS, encoding="utf-8", mode='r') as file:
                lines = file.readlines()
            sql_cmd += "AND doc:entities:cvd:esn in (\n"
            for line_item in lines[:-1]:
                esn_value = line_item.replace("\n", "")
                list_of_requested_esns.append(esn_value)
                sql_cmd += "'{}',\n".format(esn_value)
            list_of_requested_esns.append(lines[-1].replace("\n", ""))
            # Delete comma
            sql_cmd += "'{}'\n".format(list_of_requested_esns[-1])
            sql_cmd += ")\n"
        except Exception as exception:
            sys.stdout.write("\t\t\tError occurred! {}\n\n".format(exception))
    elif FILE_OF_DRIVER_IDS != '':
        try:
            with open(file=FILE_OF_DRIVER_IDS,
                    encoding="utf-8", mode='r') as file:
                lines = file.readlines()
            sql_cmd += "AND doc:entities:user:external_id in (\n"
            for line_item in lines[:-1]:
                driver_value = line_item.replace("\n", "")
                list_of_requested_esns.append(driver_value)
                sql_cmd += "'{}',\n".format(driver_value)
            list_of_requested_esns.append(lines[-1].replace("\n", ""))
            # Delete comma
            sql_cmd += "'{}'\n".format(list_of_requested_esns[-1])
            sql_cmd += ")\n"
        except Exception as exception:
            sys.stdout.write("\t\t\tError occurred! {}\n\n".format(exception))
    elif FILE_OF_POWER_UNIT_IDS != '':
        try:
            with open(file=FILE_OF_POWER_UNIT_IDS,
                      encoding="utf-8", mode='r') as file:
                lines = file.readlines()
            sql_cmd += "AND doc:entities:power_unit:power_unit_id in (\n"
            for line_item in lines[:-1]:
                esn_value = line_item.replace("\n", "")
                list_of_requested_power_units.append(esn_value)
                sql_cmd += "'{}',\n".format(esn_value)
            list_of_requested_power_units.append(lines[-1].replace("\n", ""))
            # Delete comma
            sql_cmd += "'{}'\n".format(list_of_requested_power_units[-1])
            sql_cmd += ")\n"
        except Exception as exception:
            sys.stdout.write("\t\t\tError occurred! {}\n\n".format(exception))
    elif FILE_OF_DEVICE_IDS != '':
        try:
            with open(file=FILE_OF_DEVICE_IDS, encoding="utf-8", mode='r') as file:
                lines = file.readlines()
            sql_cmd += "AND doc:entities:cvd:id in (\n"
            for line_item in lines[:-1]:
                id = line_item.replace("\n", "")
                list_of_requested_device_ids.append(id)
                sql_cmd += "'{}',\n".format(id)
            list_of_requested_device_ids.append(lines[-1].replace("\n", ""))
            # Delete comma
            sql_cmd += "'{}'\n".format(list_of_requested_device_ids[-1])
            sql_cmd += ")\n"
        except Exception as exception:
            sys.stdout.write("\t\t\tError occurred! {}\n\n".format(exception))
    elif FILTER_BY_ESN != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:entities:cvd:esn in ('{}') \n".format(FILTER_BY_ESN)
    elif FILTER_BY_TRACTOR_ID != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:entities:power_unit:power_unit_id in ('{}') \n".\
                format(FILTER_BY_TRACTOR_ID)
    elif FILTER_BY_VIN != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:entities:power_unit:vin in ('{}') \n".\
                format(FILTER_BY_VIN)
    elif FILTER_BY_TABLET_SERIAL != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:entities:tablet:serial in ('{}') \n".\
                format(FILTER_BY_TABLET_SERIAL)
    elif FILTER_BY_USER_EXTERNAL_ID != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:entities:user:external_id in ('{}') \n".\
                format(FILTER_BY_USER_EXTERNAL_ID)
    elif FILTER_BY_USER_ID != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:entities:user:id in ('{}') \n".format(FILTER_BY_USER_ID)
    if FILTER_BY_TRIP_ID != '':
        sql_cmd += "AND \n"
        sql_cmd += "CHARINDEX('{}', trip_id) > 0 \n".format(FILTER_BY_TRIP_ID)
    if FILTER_BY_CITY != '':
        sql_cmd += "AND \n"
        sql_cmd += "city in ('{}') \n".format(FILTER_BY_CITY)
    if FILTER_BY_UPM != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:upm_version:version='{}' \n".\
            format(FILTER_BY_UPM)
    if FILTER_BY_JPOD_CONFIG != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:jpod_config:version='{}' \n".\
            format(FILTER_BY_JPOD_CONFIG)
    if FILTER_BY_JPOD_FW != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:jpod_firmware:version='{}' \n".\
            format(FILTER_BY_JPOD_FW)
    if FILTER_BY_DEVICE_TYPE != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:device_type='{}' \n".\
            format(FILTER_BY_DEVICE_TYPE)
    if FILTER_BY_LMU_CFG != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:lmu_config:version='{}' \n".\
            format(FILTER_BY_LMU_CFG)
    if FILTER_BY_LMU_FW != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:lmu_firmware:version='{}' \n".\
            format(FILTER_BY_LMU_FW)
    if FILTER_BY_HAPY != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:ha_python_api_ipk:version='{}' \n".\
            format(FILTER_BY_HAPY)
    if FILTER_BY_EE != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:engine_events_ipk:version='{}' \n".\
            format(FILTER_BY_EE)

    extra_filters = filter_for_certain_values()
    sql_cmd += extra_filters

    sql_cmd += 'LIMIT {};\n'.format(LIMIT)
    rsp = execute_query(connection=snowflake_conn,
                        query=sql_cmd,
                        display_response=False)

    parsed_results = get_list_of_formatted_data(df=rsp)
    save_results_to_file(response=parsed_results, filename_prefix="HB",
            sort_keyword="CREATED_AT")
    list_to_use = list_of_requested_esns
    if FILE_OF_ESNS != "":
        list_to_use = list_of_requested_esns
        key_id_to_use = "ESN"
    elif FILE_OF_POWER_UNIT_IDS != "":
        list_to_use = list_of_requested_power_units
        key_id_to_use = "POWER_UNIT_ID"
    elif FILE_OF_DEVICE_IDS != "":
        list_to_use = list_of_requested_device_ids
        key_id_to_use = "DEVICE_ID"

    if len(list_to_use) > 0:
        sys.stdout.write("Expected {} unique responses\n".format(
            len(list_to_use)))
    if len(list_to_use) - len(parsed_results) > 0:
        sys.stdout.write("\nMissing {} ({}):\n".format(
            key_id_to_use, len(list_to_use) - len(parsed_results)))
        count = 0
        for esn in list_to_use:
            if count < 10:
                if not parsed_results['{}'.format(
                        key_id_to_use)].str.contains(esn).any():
                    count += 1
                    sys.stdout.write("\t{}\n".format(esn))
            else:
                sys.stdout.write("We have a lot missing...")
                break
        sys.stdout.write("\n")  # single '\n', not x2

    end_time = datetime.now()
    hours_min_seconds = str(
            timedelta(seconds=(end_time - start_time).seconds))
    sys.stdout.write("Response time: {} ( {} )\n\n".format(
        end_time, hours_min_seconds))

def get_doran_data():
    """
    Get Doran stuff
    """
    def print_table(rows):
        max_widths = []
        for column in zip(*rows):
            max_widths.append(max([len(text) for text in column]))
        template = '  '.join(['{{:<{}}}'.format(width) for width in max_widths])
        return '\n'.join([template.format(*row) for row in rows])

    list_of_requested_esns = []
    list_of_heartbeated_esns = []

    start_time = datetime.now()

    SEARCH_LAST_HOUR = False
    CURRENT_DATE = (datetime.now()).strftime("%Y-%m-%d").replace('-0', '-')
    global START_DATE
    START_DATE = str(START_DATE).replace('-0', '-')
    if CURRENT_DATE == START_DATE:
        SEARCH_LAST_HOUR = True
    sql_cmd = '\nSELECT \n'
    sql_cmd += 'EVENT_TYPE, \n'
    sql_cmd += 'EVENT_TIMESTAMP_UTC, \n'
    sql_cmd += 'CVD_SERIAL, \n'
    sql_cmd += 'ASSET_EXTERNAL_ID, \n'
    sql_cmd += 'TRAILER_EXTERNAL_ID, \n'
    sql_cmd += 'ASSET_ID, \n'
    sql_cmd += 'array_size(TIRES), \n'
    sql_cmd += 'TIRES[0], \n'
    sql_cmd += 'TIRES[1], \n'
    sql_cmd += 'TIRES[2], \n'
    sql_cmd += 'TIRES[3], \n'
    sql_cmd += 'TIRES[4], \n'
    sql_cmd += 'TIRES[5] \n'
    sql_cmd += '\n'
    # Updates daily at 11:33pm UTC, can request data team for immediate.
    sql_cmd += ' from "PROD"."DORAN"."FLATTENED" \n'
    #sql_cmd += ' from "INT"."DORAN"."FLATTENED" \n'
    if START_DATE and END_DATE and not SEARCH_LAST_HOUR:
        sql_cmd += 'WHERE EVENT_TIMESTAMP_UTC BETWEEN \'{}\' and \'{}\' \n'.\
                format(START_DATE, END_DATE)
    else:
        sql_cmd += "WHERE \n"
    if SEARCH_LAST_HOUR:
        #sql_cmd += 'CREATED_AT > current_timestamp - INTERVAL \'1 hour\'\n'
        sql_cmd += 'EVENT_TIMESTAMP_UTC > current_timestamp - INTERVAL \'35 minutes\'\n'
    if FILE_OF_ESNS != '':
        try:
            with open(file=FILE_OF_ESNS, encoding="utf-8", mode='r') as file:
                lines = file.readlines()
            sql_cmd += "AND doc:entities:cvd:esn in (\n"
            for line_item in lines[:-1]:
                esn_value = line_item.replace("\n", "")
                list_of_requested_esns.append(esn_value)
                sql_cmd += "'{}',\n".format(esn_value)
            list_of_requested_esns.append(lines[-1].replace("\n", ""))
            # Delete comma
            sql_cmd += "'{}'\n".format(list_of_requested_esns[-1])
            sql_cmd += ")\n"
        except Exception as exception:
            sys.stdout.write("\t\t\tError occurred! {}\n\n".format(exception))
    elif FILE_OF_DRIVER_IDS != '':
        try:
            with open(file=FILE_OF_DRIVER_IDS,
                    encoding="utf-8", mode='r') as file:
                lines = file.readlines()
            sql_cmd += "AND doc:entities:user:external_id in (\n"
            for line_item in lines[:-1]:
                driver_value = line_item.replace("\n", "")
                list_of_requested_esns.append(driver_value)
                sql_cmd += "'{}',\n".format(driver_value)
            list_of_requested_esns.append(lines[-1].replace("\n", ""))
            # Delete comma
            sql_cmd += "'{}'\n".format(list_of_requested_esns[-1])
            sql_cmd += ")\n"
        except Exception as exception:
            sys.stdout.write("\t\t\tError occurred! {}\n\n".format(exception))
    elif FILE_OF_POWER_UNIT_IDS != '':
        try:
            with open(file=FILE_OF_POWER_UNIT_IDS,
                      encoding="utf-8", mode='r') as file:
                lines = file.readlines()
            sql_cmd += "AND doc:entities:power_unit:power_unit_id in (\n"
            for line_item in lines[:-1]:
                esn_value = line_item.replace("\n", "")
                list_of_requested_esns.append(esn_value)
                sql_cmd += "'{}',\n".format(esn_value)
            list_of_requested_esns.append(lines[-1].replace("\n", ""))
            # Delete comma
            sql_cmd += "'{}'\n".format(list_of_requested_esns[-1])
            sql_cmd += ")\n"
        except Exception as exception:
            sys.stdout.write("\t\t\tError occurred! {}\n\n".format(exception))
    elif FILTER_BY_ESN != '':
        sql_cmd += "AND \n"
        sql_cmd += "CVD_SERIAL in ('{}') \n".format(FILTER_BY_ESN)
    elif FILTER_BY_TRACTOR_ID != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:entities:power_unit:power_unit_id in ('{}') \n".\
                format(FILTER_BY_TRACTOR_ID)
    elif FILTER_BY_USER_EXTERNAL_ID != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:entities:user:external_id in ('{}') \n".\
                format(FILTER_BY_USER_EXTERNAL_ID)
    elif FILTER_BY_USER_ID != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:entities:user:id in ('{}') \n".format(FILTER_BY_USER_ID)
    if FILTER_BY_UPM != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:upm_version:version='{}' \n".\
            format(FILTER_BY_UPM)
    if FILTER_BY_JPOD_CONFIG != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:jpod_config:version='{}' \n".\
            format(FILTER_BY_JPOD_CONFIG)
    if FILTER_BY_JPOD_FW != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:jpod_firmware:version='{}' \n".\
            format(FILTER_BY_JPOD_FW)
    if FILTER_BY_DEVICE_TYPE != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:device_type='{}' \n".\
            format(FILTER_BY_DEVICE_TYPE)
    if FILTER_BY_LMU_CFG != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:lmu_config:version='{}' \n".\
            format(FILTER_BY_LMU_CFG)
    if FILTER_BY_LMU_FW != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:lmu_firmware:version='{}' \n".\
            format(FILTER_BY_LMU_FW)
    if FILTER_BY_HAPY != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:ha_python_api_ipk:version='{}' \n".\
            format(FILTER_BY_HAPY)
    if FILTER_BY_EE != '':
        sql_cmd += "AND \n"
        sql_cmd += "doc:device_info:cvd_sw:engine_events_ipk:version='{}' \n".\
            format(FILTER_BY_EE)

    extra_filters = filter_for_certain_values()
    sql_cmd += extra_filters

    sql_cmd += 'LIMIT {};\n'.format(LIMIT)
    rsp = execute_query(connection=snowflake_conn,
                        query=sql_cmd,
                        display_response=False)

    parsed_results = get_list_of_formatted_data(df=rsp)
    save_results_to_file(response=parsed_results, filename_prefix="doran",
            sort_keyword="EVENT_TIMESTAMP_UTC")
    if len(list_of_requested_esns) > 0:
        sys.stdout.write("Expected {} unique responses".format(
            len(list_of_requested_esns)))
    if len(list_of_requested_esns) - len(parsed_results) > 0:
        sys.stdout.write("\nMissing ESNs ({}):".format(
            len(list_of_requested_esns) - len(parsed_results)))
        for esn in list_of_requested_esns:
            if not parsed_results['ESN'].str.contains(esn).any():
                sys.stdout.write("\t{}".format(esn))
        sys.stdout.write("")  # single '\n', not x2

    end_time = datetime.now()
    hours_min_seconds = str(
            timedelta(seconds=(end_time - start_time).seconds))
    sys.stdout.write("Response time: {} ( {} )\n".format(
        end_time, hours_min_seconds))

def get_fluent_logs():
    list_of_requested_esns = []
    list_of_requested_power_units = []
    list_of_heartbeated_esns = []
    list_of_requested_device_ids = []

    start_time = datetime.now()

    SEARCH_LAST_HOUR = False
    CURRENT_DATE = (datetime.now()).strftime("%Y-%m-%d").replace('-0', '-')
    global START_DATE
    START_DATE = str(START_DATE).replace('-0', '-')
    if CURRENT_DATE == START_DATE:
        SEARCH_LAST_HOUR = True

    sql_cmd = '\nwith fluent_logs as (\n'
    sql_cmd += 'select\n'
    sql_cmd += '    JSON,\n'
    sql_cmd += '    parse_json(JSON):log_timestamp as message_log_timestamp,\n'
    sql_cmd += '    parse_json(JSON):esn as esn,\n'
    sql_cmd += '    parse_json(JSON):log_level as log_level,\n'
    sql_cmd += '    parse_json(JSON):log_source as log_source,\n'
    sql_cmd += '    parse_json(JSON):log_message as log_message,\n'
    sql_cmd += '    parse_json(JSON):filepath as filepath,\n'
    sql_cmd += '    parse_json(JSON):log as message_raw,\n'
    sql_cmd += '    FILE_LOAD_DATETIME_UTC,\n'
    sql_cmd += 'from DEV.FLUENT.RAW_DATA\n'
    sql_cmd += ')\n'

    sql_cmd += '\nSELECT \n'
    sql_cmd += '*,\n'
    sql_cmd += 'from fluent_logs\n'
    if START_DATE and END_DATE and not SEARCH_LAST_HOUR:
        sql_cmd += 'WHERE fluent_logs.FILE_LOAD_DATETIME_UTC BETWEEN '\
                '\'{}\' and \'{}\' \n'.format(START_DATE, END_DATE)
    else:
        sql_cmd += "WHERE \n"
    if SEARCH_LAST_HOUR:
        # Interval options of type: '35 minutes', '1 hour', etc
        sql_cmd += 'fluent_logs.FILE_LOAD_DATETIME_UTC > '\
            'current_timestamp - INTERVAL \'35 minutes\'\n'
    if FILTER_BY_ENVIRONMENT != '':
        if START_DATE and END_DATE:
            sql_cmd += 'AND \n'
        else:
            sql_cmd += 'WHERE \n'
        sql_cmd += 'utils.deployment_id_to_customer_name(deployment_id) '\
                   '= \'{}\' \n'.format(FILTER_BY_ENVIRONMENT)
    elif FILTER_BY_ESN != '':
        sql_cmd += "AND esn in ('{}') \n".format(FILTER_BY_ESN)

    extra_filters = filter_for_certain_values_go_fast()
    sql_cmd += extra_filters

    sql_cmd += 'LIMIT {};\n'.format(LIMIT)
    rsp = execute_query(connection=snowflake_conn,
                        query=sql_cmd,
                        display_response=False)

    parsed_results = get_list_of_formatted_data(df=rsp)
    save_results_to_file(response=parsed_results, filename_prefix="FLUENT",
            sort_keyword="FILE_LOAD_DATETIME_UTC")
    list_to_use = list_of_requested_esns
    if FILE_OF_ESNS != "":
        list_to_use = list_of_requested_esns
        key_id_to_use = "ESN"
    elif FILE_OF_POWER_UNIT_IDS != "":
        list_to_use = list_of_requested_power_units
        key_id_to_use = "POWER_UNIT_ID"
    elif FILE_OF_DEVICE_IDS != "":
        list_to_use = list_of_requested_device_ids
        key_id_to_use = "DEVICE_ID"

    if len(list_to_use) > 0:
        sys.stdout.write("Expected {} unique responses\n".format(
            len(list_to_use)))
    if len(list_to_use) - len(parsed_results) > 0:
        sys.stdout.write("\nMissing {} ({}):\n".format(
            key_id_to_use, len(list_to_use) - len(parsed_results)))
        count = 0
        for esn in list_to_use:
            if count < 10:
                if not parsed_results['{}'.format(
                        key_id_to_use)].str.contains(esn).any():
                    count += 1
                    sys.stdout.write("\t{}\n".format(esn))
            else:
                sys.stdout.write("We have a lot missing...")
                break
        sys.stdout.write("\n")  # single '\n', not x2

    end_time = datetime.now()
    hours_min_seconds = str(
            timedelta(seconds=(end_time - start_time).seconds))
    sys.stdout.write("Response time: {} ( {} )\n\n".format(
        end_time, hours_min_seconds))

def convert_power_ids_to_esn():
    """
    Given a list of power unit ids, convert them to ESNs.
    """
    list_of_requested_esns = []

    sql_cmd = '\nSELECT \n\
    POWER_UNIT, ESN FROM "PROD"."HEARTBEAT_MODELS"."FLATTENED"\n\
    WHERE 1=1\n'

    SEARCH_LAST_HOUR = False
    CURRENT_DATE = (datetime.now()).strftime("%Y-%m-%d").replace('-0', '-')

    global START_DATE
    START_DATE = str(START_DATE).replace('-0', '-')
    if CURRENT_DATE == START_DATE:
        SEARCH_LAST_HOUR = True

    if START_DATE and END_DATE and not SEARCH_LAST_HOUR:
        sql_cmd += 'AND created_at BETWEEN \'{}\' and \'{}\' \n'.format(
                START_DATE, END_DATE)

    if FILE_OF_POWER_UNIT_IDS != '':
        try:
            with open(file=FILE_OF_POWER_UNIT_IDS,
                      encoding="utf-8", mode='r') as file:
                lines = file.readlines()
            sql_cmd += "AND power_unit in (\n"
            for line_item in lines[:-1]:
                esn_value = line_item.replace("\n", "")
                list_of_requested_esns.append(esn_value)
                sql_cmd += "'{}',\n".format(esn_value)
            list_of_requested_esns.append(lines[-1].replace("\n", ""))
            # Delete comma
            sql_cmd += "'{}'\n".format(list_of_requested_esns[-1])
            sql_cmd += ")\n"
        except Exception as exception:
            sys.stdout.write("\t\t\tError occurred! {}\n\n".format(exception))
    sql_cmd += 'LIMIT {};\n'.format(LIMIT)
    rsp = execute_query(connection=snowflake_conn,
                        query=sql_cmd,
                        display_response=False)
    rsp_list_of_esns = rsp.ESN.to_list()
    rsp_list_of_power_unit_ids = rsp.POWER_UNIT.to_list()
    displayed_power_unit_list = []
    filtered_rsp = []
    for index, line in enumerate(rsp_list_of_power_unit_ids):
        power_id = rsp_list_of_power_unit_ids[index].replace('\n', '')
        esn = rsp_list_of_esns[index].replace('\n', '')
        if power_id not in displayed_power_unit_list:
            displayed_power_unit_list.append(power_id)
            filtered_rsp.append(power_id + ' - ' + esn)
    for index, row in enumerate(filtered_rsp):
        sys.stdout.write("{} - {}".format(index, row))
    save_results_to_file(response=rsp)


def get_sherlock_data():
    """
    Get Sherlock data.
    """
    def print_table(rows):
        max_widths = []
        for column in zip(*rows):
            max_widths.append(max([len(text) for text in column]))
        template = '  '.join(['{{:<{}}}'.format(width) for width in max_widths])
        return '\n'.join([template.format(*row) for row in rows])

    start_time = datetime.now()
    #sql_cmd = '\nSELECT *, json:data:attributes:esn::varchar as esn '\
    #          'from "PROD"."PUBLIC"."CVD_HEALTH_LOGS_V2_RAW_LOGS"\n'
    sql_cmd = '\nSELECT *, esn '\
              'from "PROD"."ENTITY_HEALTH"."CVD_HEALTH_LOGS_FLATTENED"\n'

    if START_DATE and END_DATE:
        sql_cmd += 'WHERE FILE_LOAD_DATETIME_UTC BETWEEN \'{}\' and \'{}\' \n'.format(
                START_DATE, END_DATE)
    else:
        sql_cmd += 'WHERE FILE_LOAD_DATETIME_UTC BETWEEN \n'\
                   '  TODATE(dateadd(\'day\', -5, current_timestamp)) \n'\
                   '  AND current_date\n'

    if FILE_OF_ESNS != '':
        try:
            with open(file=FILE_OF_ESNS, encoding="utf-8", mode='r') as file:
                lines = file.readlines()
            sql_cmd += "AND ESN in (\n"
            list_of_requested_esns = []
            for line_item in lines[:-1]:
                esn_value = line_item.replace("\n", "")
                list_of_requested_esns.append(esn_value)
                sql_cmd += "'{}',\n".format(esn_value)
            list_of_requested_esns.append(lines[-1].replace("\n", ""))
            sql_cmd += "'{}'\n".format(list_of_requested_esns[-1])  # Delete comma
            sql_cmd += ")\n"
        except Exception as exception:
            sys.stdout.write("\t\t\tError occurred! {}\n\n".format(exception))
    elif FILTER_BY_ESN != '':
        sql_cmd += 'AND ESN = \'{}\'\n'.format(FILTER_BY_ESN)

    extra_filters = filter_for_certain_values()
    sql_cmd += extra_filters
    sql_cmd += 'ORDER BY FILE_LOAD_DATETIME_UTC desc\n'

    sql_cmd += 'LIMIT {};\n'.format(LIMIT)
    rsp = execute_query(connection=snowflake_conn,
                        query=sql_cmd,
                        display_response=False)
    rsp = move_column_around(df_doc=rsp,
            string="UBOOT_LMU_APP_CRASH",
            new_name="UBOOT_LMU_APP_CRASH")
    rsp = move_column_around(df_doc=rsp,
            string="UBOOT_LINUXBOOT",
            new_name="UBOOT_LINUXBOOT")
    rsp = move_column_around(df_doc=rsp,
            string="FAILSAFE",
            new_name="FAILSAFE")
    rsp = move_column_around(df_doc=rsp,
            string="FAILSAFE_STATE",
            new_name="FAILSAFE_STATE")
    rsp = move_column_around(df_doc=rsp,
            string="EVENT_TIMESTAMP_UTC",
            new_name="EVENT_TIMESTAMP_UTC")
    save_results_to_file(response=rsp, filename_prefix="sherlock",
            sort_keyword="FILE_LOAD_DATETIME_UTC")

    end_time = datetime.now()
    query_time = (end_time - start_time).seconds
    sys.stdout.write("\nResponse: {} took {} seconds\n".format(
        end_time, query_time))

    return rsp


def get_hos_logs():
    """
    Get HOS logs.
    """
    def print_table(rows):
        max_widths = []
        for column in zip(*rows):
            max_widths.append(max([len(text) for text in column]))
        template = '  '.join(['{{:<{}}}'.format(width)
                             for width in max_widths])
        return '\n'.join([template.format(*row) for row in rows])

    start_time = datetime.now()
    SEARCH_LAST_HOUR = False
    CURRENT_DATE = (datetime.now()).strftime("%Y-%m-%d").replace('-0', '-')
    global START_DATE
    START_DATE = str(START_DATE).replace('-0', '-')
    if CURRENT_DATE == START_DATE:
        SEARCH_LAST_HOUR = True

    sql_cmd = '\nSELECT * from "PROD"."ORION"."HOS_EVENT_LOG" \n'
    #sql_cmd = '\nSELECT * from "PROD"."ORION"."HOS_LOGS" \n'
    #if START_DATE and END_DATE and not SEARCH_LAST_HOUR:
    if START_DATE and END_DATE:
        sql_cmd += 'WHERE created_at BETWEEN \'{}\' and \'{}\' \n'.format(
                START_DATE, END_DATE)

    if FILE_OF_POWER_UNIT_IDS != '':
        list_of_requested_esns = []
        try:
            with open(file=FILE_OF_POWER_UNIT_IDS,
                      encoding="utf-8", mode='r') as file:
                lines = file.readlines()
            sql_cmd += "AND POWER_UNIT_NUMBER in (\n"
            for line_item in lines[:-1]:
                esn_value = line_item.replace("\n", "")
                list_of_requested_esns.append(esn_value)
                sql_cmd += "'{}',\n".format(esn_value)
            list_of_requested_esns.append(lines[-1].replace("\n", ""))
            # Delete comma
            sql_cmd += "'{}'\n".format(list_of_requested_esns[-1])  
            sql_cmd += ")\n"
        except Exception as exception:
            sys.stdout.write("\t\t\tError occurred! {}\n\n".format(exception))
    elif FILTER_BY_TRACTOR_ID != '':
        sql_cmd += 'AND POWER_UNIT_NUMBER = \'{}\'\n'.\
                format(FILTER_BY_TRACTOR_ID)
    if FILTER_BY_VIN != '':
        sql_cmd += 'AND VIN= \'{}\'\n'.\
                format(FILTER_BY_VIN)
    if FILTER_BY_ENVIRONMENT != '':
        sql_cmd += 'AND utils.deployment_id_to_customer_name(deployment_id) '\
                   '= \'{}\' \n'.format(FILTER_BY_ENVIRONMENT)

    DEBUG_SEARCH_FOR_DDE = True

    if DEBUG_SEARCH_FOR_DDE:
        # See Confluence page 'HOS Compliance and Health" for more details.
        HOS_EVENT_TYPE_CODE_LOOKUP = {
                "Engine power on w/ conventional location precision": (6,1),
                "Engine power on w/ reduced location precision" : (6,2),
                "Engine power off w/ conventional location precision" : (6,3),
                "Engine power off w/ reduced location precision" : (6,4),
                "ELD Malfunction Logged": (7,1),
                "ELD Malfunction Cleared": (7,2),
                "Data Diagnostic Event Logged": (7,3),
                "Data Diagnostic Event Cleared": (7,4),
        }
        malfunction_code = None
        #malfunction_code = 'E'
        #malfunction_code = 'L'

        event_type = 0
        event_code = 0
        #event_type_string = "Engine power off w/ reduced location precision"
        event_type_string = "ELD Malfunction Logged"
        #event_type_string = "ELD Malfunction Cleared"
        #event_type_string = "Data Diagnostic Event Logged"
        event_type = HOS_EVENT_TYPE_CODE_LOOKUP[event_type_string][0]
        event_code = HOS_EVENT_TYPE_CODE_LOOKUP[event_type_string][1]

        if malfunction_code:
            sql_cmd += 'AND MALFUNCTION_CODE= \'{}\'\n'.\
                    format(malfunction_code)
        if event_type != 0 and event_code != 0:
            sql_cmd += 'AND EVENT_TYPE= \'{}\'\n'.\
                    format(event_type)
            sql_cmd += 'AND EVENT_CODE= \'{}\'\n'.\
                    format(event_code)


    sql_cmd += 'LIMIT {};\n'.format(LIMIT)
    rsp = execute_query(connection=snowflake_conn,
                        query=sql_cmd,
                        display_response=False)

    def convert_event_code_type_to_description(event_type, event_code):
        """
        Human readable is grabbed from Confluence page:
            "HOS Compliance and Health"
        https://pltsci.atlassian.net/wiki/spaces/DT/pages/2111832354/\
                HOS+Compliance+and+Health 
        """
        human_description = "Unknown"
        if event_type == 1:
            if event_code == 1:
                human_description = "Off-duty"
            elif event_code == 2:
                human_description = "Sleeper berth"
            elif event_code == 3:
                human_description = "Driving"
            elif event_code == 4:
                human_description = "On-duty not driving"
        elif event_type == 2:
            if event_code == 1:
                human_description = "Intermediate log w/ conventional"\
                        " location precision"
            elif event_code == 2:
                human_description = "Intermediate log w/ reduced location"\
                        " precision"
        elif event_type == 3:
            if event_code == 0:
                human_description = "PM, YM, or WT cleared"
            elif event_code == 1:
                human_description = "Personal conveyance of CMV"
            elif event_code == 2:
                human_description = "Yard move"
        elif event_type == 4:
            if event_code == 1:
                human_description = "First certification of a daily record"
            else:
                human_description = "{} certification of a daily record".\
                        format(event_code)
        elif event_type == 5:
            if event_code == 1:
                human_description = "Driver ELD login"
            elif event_code == 2:
                human_description = "Driver ELD logout"
        elif event_type == 6:
            if event_code == 1:
                human_description = "Engine power on w/ conventional "\
                        "location precision"
            elif event_code == 2:
                human_description = "Engine power on w/ reduced location "\
                        "precision"
            elif event_code == 3:
                human_description = "Engine power off w/ conventional "\
                        "location precision"
            elif event_code == 4:
                human_description = "Engine power off w/ reduced location "\
                        "precision"
        elif event_type == 7:
            if event_code == 1:
                human_description = "ELD malfunction logged"
            elif event_code == 2:
                human_description = "ELD malfunction cleared"
            elif event_code == 3:
                human_description = "Data diagnostic event logged"
            elif event_code == 4:
                human_description = "Data diagnostic event cleared"


        return human_description

    try:
        rsp['Event Code Description'] = rsp.apply(
                lambda row: convert_event_code_type_to_description(
                    row['EVENT_TYPE'], row['EVENT_CODE']), axis=1)
    except:
        pass
    rsp = move_column_around(df_doc=rsp,
            string="MALFUNCTION_CODE",
            new_name="MALFUNCTION_CODE")
    rsp = move_column_around(df_doc=rsp,
            string="Event Code Description",
            new_name="Event Code Description")
    rsp = move_column_around(df_doc=rsp,
            string="EVENT_CODE",
            new_name="EVENT_CODE")
    rsp = move_column_around(df_doc=rsp,
            string="EVENT_TYPE",
            new_name="EVENT_TYPE")
    rsp = move_column_around(df_doc=rsp,
            string="POWER_UNIT_NUMBER",
            new_name="POWER_UNIT_NUMBER")
    rsp = move_column_around(df_doc=rsp,
            string="CREATED_AT",
            new_name="CREATED_AT")

    if (FILE_OF_ESNS != ''
            or FILE_OF_DRIVER_IDS != ''
            or FILE_OF_POWER_UNIT_IDS != ''
            or FILE_OF_DEVICE_IDS != ''
        or (FILTER_BY_ESN == ''
            and FILTER_BY_USER_EXTERNAL_ID == ''
            and FILTER_BY_TRACTOR_ID == ''
            and FILTER_BY_VIN == ''
            and FILTER_BY_DEVICE_ID == ''
            and FILTER_BY_USER_ID == '')
        ):
            rsp = rsp.sort_values(by="CREATED_AT", ascending=False)
            if True:
                sys.stdout.write("Filtering out for unique responses...\n")
                rsp = rsp.drop_duplicates(subset=['POWER_UNIT_NUMBER'])

    save_results_to_file(response=rsp, filename_prefix="hos",
            sort_keyword="CREATED_AT")

    end_time = datetime.now()
    query_time = (end_time - start_time).seconds
    sys.stdout.write("\nResponse: {} took {} seconds\n".format(
        end_time, query_time))

    return rsp


def get_driver_performance():
    """
    Get driver performance.
    """
    def print_table(rows):
        max_widths = []
        for column in zip(*rows):
            max_widths.append(max([len(text) for text in column]))
        template = '  '.join(['{{:<{}}}'.format(width)
                             for width in max_widths])
        return '\n'.join([template.format(*row) for row in rows])

    start_time = datetime.now()
    SEARCH_LAST_HOUR = False
    CURRENT_DATE = (datetime.now()).strftime("%Y-%m-%d").replace('-0', '-')
    global START_DATE
    START_DATE = str(START_DATE).replace('-0', '-')
    if CURRENT_DATE == START_DATE:
        SEARCH_LAST_HOUR = True

    if USE_DEV_ENV == 'yes':
        sql_cmd = '\nSELECT * from "DEV"."ORION"."DRIVER_PERFORMANCE" \n'
#    else:
#        sql_cmd = '\nSELECT\n'\
#                'device_id,\n'\
#                'driver_performance.created_at,\n'\
#                'asset_id,\n'\
#                'ASSETS.EXTERNAL_ID,\n'\
#                'DATA_EXTRACT\n'\
#                'from \n\tPROD.ORION.DRIVER_PERFORMANCE\n'
#        sql_cmd += '\tleft join UAT.ORION.ASSETS \n'\
#                'on DRIVER_PERFORMANCE.ASSET_ID = ASSETS.ID\n'
    else:
        sql_cmd = '\nSELECT * from "PROD"."ORION"."DRIVER_PERFORMANCE" \n'
    if START_DATE and END_DATE and not SEARCH_LAST_HOUR:
        sql_cmd += 'WHERE START_TIME BETWEEN \'{}\' and \'{}\' \n'.format(
                START_DATE, END_DATE)
    else:
        sql_cmd += 'WHERE START_TIME BETWEEN \n'\
                   '  dateadd(day, -1, current_timestamp) \n'\
                   '  AND current_timestamp\n'
    if FILE_OF_POWER_UNIT_IDS != '':
        try:
            list_of_requested_power_units = []
            with open(file=FILE_OF_POWER_UNIT_IDS,
                      encoding="utf-8", mode='r') as file:
                lines = file.readlines()
            sql_cmd += "AND EXTERNAL_ID in (\n"
            for line_item in lines[:-1]:
                power_unit = line_item.replace("\n", "")
                list_of_requested_power_units.append(power_unit)
                sql_cmd += "'{}',\n".format(power_unit)
            list_of_requested_power_units.append(lines[-1].replace("\n", ""))
            # Delete comma
            sql_cmd += "'{}'\n".format(list_of_requested_power_units[-1])
            sql_cmd += ")\n"
        except Exception as exception:
            sys.stdout.write("\t\t\tError occurred! {}\n\n".format(exception))
    if FILE_OF_DEVICE_IDS != '':
        # This is from DOC.entities.cvd.id
        try:
            list_of_requested_power_units = []
            with open(file=FILE_OF_DEVICE_IDS,
                      encoding="utf-8", mode='r') as file:
                lines = file.readlines()
            sql_cmd += "AND DEVICE_ID in (\n"
            for line_item in lines[:-1]:
                power_unit = line_item.replace("\n", "")
                list_of_requested_power_units.append(power_unit)
                sql_cmd += "'{}',\n".format(power_unit)
            list_of_requested_power_units.append(lines[-1].replace("\n", ""))
            # Delete comma
            sql_cmd += "'{}'\n".format(list_of_requested_power_units[-1])
            sql_cmd += ")\n"
        except Exception as exception:
            sys.stdout.write("\t\t\tError occurred! {}\n\n".format(exception))

    if FILTER_BY_ENVIRONMENT != '':
        sql_cmd += 'AND utils.deployment_id_to_customer_name('\
                'deployment_id) = \'{}\' \n'.format(FILTER_BY_ENVIRONMENT)
    if FILTER_BY_USER_ID != '':
        sql_cmd += 'AND USER_ID = \'{}\'\n'.\
                format(FILTER_BY_USER_ID)
    if FILTER_BY_DEVICE_ID != '':
        sql_cmd += 'AND DEVICE_ID = \'{}\'\n'.\
                format(FILTER_BY_DEVICE_ID)
    if FILTER_BY_TRACTOR_ID != '':
        sql_cmd += "AND \n"
        sql_cmd += "POWER_UNIT_ID in ('{}') \n".\
                format(FILTER_BY_TRACTOR_ID)

    FILTER_BY_MPG_REQUIREMENTS = False
    if FILTER_BY_MPG_REQUIREMENTS:
        sql_cmd += 'AND DATA_EXTRACT:distance_drive > 20 \n'
        #sql_cmd += 'AND DATA_EXTRACT:total_fuel_used > 0 \n'
        # MPG Filter
        #sql_cmd += 'AND Round(DATA_EXTRACT:distance_drive / '\
        #    'DATA_EXTRACT:total_fuel_used, 2) < 10.0 \n'

    ######################## KAG Exception Rules #############################
    sql_cmd += 'AND(\n'
    FILTER_BY_MPG_REQUIREMENTS_KAG_1 = True
    if FILTER_BY_MPG_REQUIREMENTS_KAG_1:
        # No Distance
        sql_cmd += '\tDATA_EXTRACT:moving_time > 10 \n'
        sql_cmd += '\tAND DATA_EXTRACT:distance_drive = 0 \n'

    FILTER_BY_MPG_REQUIREMENTS_KAG_2 = True
    if FILTER_BY_MPG_REQUIREMENTS_KAG_2:
        # No Fuel
        sql_cmd += 'OR (\n'
        sql_cmd += '\tDATA_EXTRACT:distance_drive >= 5 \n'
        sql_cmd += '\tAND DATA_EXTRACT:total_fuel_used = 0 \n'
        sql_cmd += ')\n'

    FILTER_BY_MPG_REQUIREMENTS_KAG_3 = True
    if FILTER_BY_MPG_REQUIREMENTS_KAG_3:
        # No Engine Time
        sql_cmd += 'OR (\n'
        sql_cmd += '\tDATA_EXTRACT:moving_time >= 1 \n'
        sql_cmd += '\tAND DATA_EXTRACT:engine_time = 0 \n'
        sql_cmd += ')\n'

    FILTER_BY_MPG_REQUIREMENTS_KAG_4 = True
    if FILTER_BY_MPG_REQUIREMENTS_KAG_4:
        # No Move Time
        sql_cmd += 'OR (\n'
        sql_cmd += '\tDATA_EXTRACT:distance_drive >= 5 \n'
        sql_cmd += '\tAND DATA_EXTRACT:moving_time = 0 \n'
        sql_cmd += ')\n'

    FILTER_BY_MPG_REQUIREMENTS_KAG_5 = True
    if FILTER_BY_MPG_REQUIREMENTS_KAG_5:
        # Exception Engine Time:
        #   EngineTime > (DataStartUTC to DataEndUTC + 10 minutes)
        sql_cmd += 'OR (\n'
        sql_cmd += '\tSTART_TIME < END_TIME \n'
        sql_cmd += '\tAND DATA_EXTRACT:engine_time - 10 >= '\
                + 'DATEDIFF(second, START_TIME, END_TIME) / 60  \n'
        sql_cmd += ')\n'

    FILTER_BY_MPG_REQUIREMENTS_KAG_6 = True
    if FILTER_BY_MPG_REQUIREMENTS_KAG_6:
        # Exception Moving Time
        sql_cmd += 'OR (\n'
        sql_cmd += '\tDATA_EXTRACT:moving_time > DATA_EXTRACT:engine_time \n'
        sql_cmd += ')\n'

    FILTER_BY_MPG_REQUIREMENTS_KAG_7 = True
    if FILTER_BY_MPG_REQUIREMENTS_KAG_7:
        # Exception Distance
        sql_cmd += 'OR (\n'
        sql_cmd += '\tDATA_EXTRACT:distance_drive > 10000\n'
        sql_cmd += ')\n'

    FILTER_BY_MPG_REQUIREMENTS_KAG_8 = True
    if FILTER_BY_MPG_REQUIREMENTS_KAG_8:
        # Exception Park Idle Fuel
        # park idle fuel = stop_idle_fuel_used
        sql_cmd += 'OR (\n'
        sql_cmd += '\tDATA_EXTRACT:distance_drive > 0\n'
        sql_cmd += '\tAND DATA_EXTRACT:stop_idle_fuel_used'\
                + '> DATA_EXTRACT:total_fuel_used\n'
        sql_cmd += ')\n'

    FILTER_BY_MPG_REQUIREMENTS_KAG_8 = True
    if FILTER_BY_MPG_REQUIREMENTS_KAG_8:
        # Exception Idle Fuel
        sql_cmd += 'OR (\n'
        sql_cmd += '\tDATA_EXTRACT:total_idle_fuel_used '\
                + '> DATA_EXTRACT:total_fuel_used\n'
        sql_cmd += ')\n'

    FILTER_BY_MPG_REQUIREMENTS_KAG_TIME_WRONG = True
    if FILTER_BY_MPG_REQUIREMENTS_KAG_TIME_WRONG:
        # Exception Engine Time:
        #   EngineTime > (DataStartUTC to DataEndUTC + 10 minutes)
        sql_cmd += 'OR (\n'
        sql_cmd += '\tSTART_TIME > END_TIME \n'
        sql_cmd += ')\n'
    sql_cmd += ')\n'

    ######################## KAG Exception Rules #############################
 
    FILTER_BY_ENGINE_LOAD = False
    if FILTER_BY_ENGINE_LOAD:
        #sql_cmd += 'AND DATA_EXTRACT:avg_engine_load > 40 \n'
        sql_cmd += 'AND DATA_EXTRACT:distance_drive < 1000 \n'
        sql_cmd += 'AND DATA_EXTRACT:distance_drive > 15 \n'
        #sql_cmd += 'AND DATA_EXTRACT:cruise_control_time < 5 \n'


    sql_cmd += 'LIMIT {};\n'.format(LIMIT)
    rsp = execute_query(connection=snowflake_conn,
                        query=sql_cmd,
                        display_response=False)

    # Flatten values from 'DATA_EXTRACT'
    df = pd.json_normalize(rsp["DATA_EXTRACT"].apply(only_dict).\
            tolist()).add_prefix("DATA_EXTRACT.")

    if (len(df) > 0
            and FILTER_BY_DEVICE_ID != ''):
        # Only compute sum if we have a single ESN
        df['CALCULATED.distance_drive.sum'] = \
                df['DATA_EXTRACT.distance_drive'].sum()
        df['CALCULATED.total_fuel_used.sum'] = \
                df['DATA_EXTRACT.total_fuel_used'].sum()

        # Calculate MPG:
        df["CALCULATED.MPG.distance_drive.total_fuel_used"] = \
                (df["DATA_EXTRACT.distance_drive"] /
                            df["DATA_EXTRACT.total_fuel_used"])

        # Calculate Idle Time Percentage:
        df["CALCULATED.IdleTimePercentage"] = \
                100*(df["DATA_EXTRACT.total_idle_time"] /
                            df["DATA_EXTRACT.engine_time"])


    try:
        rsp = rsp[[
            'ASSET_ID',
            'CREATED_AT',
            #'DELETED_AT',
            'DEPLOYMENT_ID',
            'DEVICE_ID',
            'END_TIME',
            #'ID',
            'START_TIME',
            #'UPDATED_AT', 
            'USER_ID',
            'UUID'
            ]].join([df])
    except Exception as e:
        sys.stdout.write("Could not find key...{}".format(e))

    # Convert deployment ID to something human readable:
    if 'DEPLOYMENT_ID' in rsp:
        rsp['ENV'] = rsp.apply(
                lambda row: convert_deployment_id_to_env(
                    row['DEPLOYMENT_ID']), axis=1)

    # Filter for unique responses
    if (FILE_OF_ESNS != ''
            or FILE_OF_DRIVER_IDS != ''
            or FILE_OF_POWER_UNIT_IDS != ''
            or FILE_OF_DEVICE_IDS != ''
            or FILTER_BY_ESN == ''
            or FILTER_BY_DEVICE_ID == ''):
        # Filter out unique hits
        rsp = rsp.sort_values(by="CREATED_AT", ascending=False)
        filter_out_unique_hits = True
        if filter_out_unique_hits and FILTER_BY_DEVICE_ID == '':
            rsp = rsp.drop_duplicates(subset=['DEVICE_ID'])

    # Delete unnecessary columns:
    def delete_from_column(string=""):
        if string in rsp:
            del rsp[string]
    delete_from_column("DELETED_AT")
    #delete_from_column("DATA_EXTRACT.avg_engine_load")
    #delete_from_column("DATA_EXTRACT.coast_out_gear_time")
    #delete_from_column("DATA_EXTRACT.cruise_control_time")
    #delete_from_column("DATA_EXTRACT.engine_readings_count")
    #delete_from_column("DATA_EXTRACT.engine_time")
    #delete_from_column("DATA_EXTRACT.drive_time")
    #delete_from_column("DATA_EXTRACT.excessive_overspeed_count")
    #delete_from_column("DATA_EXTRACT.excessive_overspeed_time")
    #delete_from_column("DATA_EXTRACT.excessive_rpm_count")
    #delete_from_column("DATA_EXTRACT.excessive_rpm_time")
    #delete_from_column("DATA_EXTRACT.expected_end_datetime")
    #delete_from_column("DATA_EXTRACT.gear_data_source")
    #delete_from_column("DATA_EXTRACT.intertrip_idle_time")
    #delete_from_column("DATA_EXTRACT.moving_fuel_used")
    #delete_from_column("DATA_EXTRACT.moving_time")
    #delete_from_column("DATA_EXTRACT.over_rpm_count")
    #delete_from_column("DATA_EXTRACT.over_rpm_max_duration")
    #delete_from_column("DATA_EXTRACT.over_rpm_max_value")
    #delete_from_column("DATA_EXTRACT.over_rpm_time")
    #delete_from_column("DATA_EXTRACT.over_speed_count")
    #delete_from_column("DATA_EXTRACT.over_speed_max_duration")
    #delete_from_column("DATA_EXTRACT.over_speed_time")
    #delete_from_column("DATA_EXTRACT.performance_group_id")
    #delete_from_column("DATA_EXTRACT.pto_fuel_used")
    #delete_from_column("DATA_EXTRACT.pto_time")
    #delete_from_column("DATA_EXTRACT.short_idle_count")
    #delete_from_column("DATA_EXTRACT.short_idle_fuel_used")
    #delete_from_column("DATA_EXTRACT.short_idle_time")
    #delete_from_column("DATA_EXTRACT.speed_rpm_matrix")
    #delete_from_column("DATA_EXTRACT.stop_idle_count")
    #delete_from_column("DATA_EXTRACT.stop_idle_fuel_used")
    #delete_from_column("DATA_EXTRACT.stop_idle_time")
    #delete_from_column("DATA_EXTRACT.stop_idle_warning_count")
    #delete_from_column("DATA_EXTRACT.top_gear_time")
    #delete_from_column("DATA_EXTRACT.total_idle_fuel_used")
    #delete_from_column("DATA_EXTRACT.total_idle_time")
    #delete_from_column("DATA_EXTRACT.total_trip_count")
    #delete_from_column("DATA_EXTRACT.odometer_start")
    #delete_from_column("DEPLOYMENT_ID")

    rsp = move_column_around(df_doc=rsp,
            string="CALCULATED.MPG.distance_drive.total_fuel_used",
            new_name="CALCULATED.MPG.distance_drive.total_fuel_used")
    rsp = move_column_around(df_doc=rsp,
            string="DATA_EXTRACT.total_idle_fuel_used",
            new_name="DATA_EXTRACT.total_idle_fuel_used")
    rsp = move_column_around(df_doc=rsp,
            string="DATA_EXTRACT.stop_idle_fuel_used",
            new_name="DATA_EXTRACT.stop_idle_fuel_used")
    rsp = move_column_around(df_doc=rsp,
            string="DATA_EXTRACT.total_fuel_used",
            new_name="DATA_EXTRACT.total_fuel_used")
    rsp = move_column_around(df_doc=rsp,
            string="DATA_EXTRACT.odometer_start",
            new_name="DATA_EXTRACT.odometer_start")
    rsp = move_column_around(df_doc=rsp,
            string="DATA_EXTRACT.drive_time",
            new_name="DATA_EXTRACT.drive_time")
    rsp = move_column_around(df_doc=rsp,
            string="CALCULATED.IdleTimePercentage",
            new_name="CALCULATED.IdleTimePercentage")
    rsp = move_column_around(df_doc=rsp,
            string="DATA_EXTRACT.engine_time",
            new_name="DATA_EXTRACT.engine_time")
    rsp = move_column_around(df_doc=rsp,
            string="DATA_EXTRACT.moving_time",
            new_name="DATA_EXTRACT.moving_time")
    rsp = move_column_around(df_doc=rsp,
            string="DATA_EXTRACT.distance_drive",
            new_name="DATA_EXTRACT.distance_drive")
    rsp = move_column_around(df_doc=rsp,
            string="USER_ID",
            new_name="USER_ID")
    rsp = move_column_around(df_doc=rsp,
            string="DEVICE_ID",
            new_name="DEVICE_ID")
    rsp = move_column_around(df_doc=rsp,
            string="END_TIME",
            new_name="END_TIME")
    rsp = move_column_around(df_doc=rsp,
            string="START_TIME",
            new_name="START_TIME")
    rsp = move_column_around(df_doc=rsp,
            string="CREATED_AT",
            new_name="CREATED_AT")
    rsp = move_column_around(df_doc=rsp,
            string="ENV",
            new_name="ENV")

    save_results_to_file(response=rsp, filename_prefix="dpm",
            sort_keyword="START_TIME")

    end_time = datetime.now()
    query_time = (end_time - start_time).seconds
    sys.stdout.write("\nResponse: {} took {} seconds\n".format(end_time, query_time))

    return rsp

def get_playground_data():
    """
    get_playground_data()
    """
    sql_cmd = "WITH consecutive_hbs AS (\n"\
        "SELECT                                                                                                            \n"\
        "     heartbeat_id,                                                                                                \n"\
        "     logged_at                                                                                                    \n"\
        "     created_at,                                                                                                  \n"\
        "     prod.utils.deployment_id_to_customer_name(deployment_id) as customer,                                        \n"\
        "     cvd_id,                                                                                                      \n"\
        "     esn,                                                                                                         \n"\
        "     engine_events_version,                                                                                       \n"\
        "     engine_events_ipk_version,                                                                                   \n"\
        "     event,                                                                                                       \n"\
        "     shutdown_reason,                                                                                             \n"\
        "     odometer,                                                                                                    \n"\
        "     cvd_voltage,                                                                                                 \n"\
        "     LEAD(logged_at) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_logged_at,          \n"\
        "     LEAD(heartbeat_id) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_heartbeat_id,    \n"\
        "     LEAD(cvd_voltage) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_cvd_voltage,      \n"\
        "     LEAD(event) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_event,                  \n"\
        "     LEAD(logged_at,2) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_logged_at_2,      \n"\
        "     LEAD(heartbeat_id,2) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_heartbeat_id_2,\n"\
        "     LEAD(cvd_voltage,2) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_cvd_voltage_2,  \n"\
        "     LEAD(event,2) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_event_2,              \n"\
        "     LEAD(logged_at,3) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_logged_at_3,      \n"\
        "     LEAD(heartbeat_id,3) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_heartbeat_id_3,\n"\
        "     LEAD(cvd_voltage,3) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_cvd_voltage_3,  \n"\
        "     LEAD(event,3) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_event_3,              \n"\
        "     LEAD(logged_at,4) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_logged_at_4,      \n"\
        "     LEAD(heartbeat_id,4) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_heartbeat_id_4,\n"\
        "     LEAD(cvd_voltage,4) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_cvd_voltage_4,  \n"\
        "     LEAD(event,4) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_event_4,              \n"\
        "     LEAD(logged_at,5) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_logged_at_5,      \n"\
        "     LEAD(heartbeat_id,5) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_heartbeat_id_5,\n"\
        "     LEAD(cvd_voltage,5) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_cvd_voltage_5,  \n"\
        "     LEAD(event,5) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_event_5,              \n"\
        "     LEAD(logged_at,6) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_logged_at_6,      \n"\
        "     LEAD(heartbeat_id,6) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_heartbeat_id_6,\n"\
        "     LEAD(cvd_voltage,6) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_cvd_voltage_6,  \n"\
        "     LEAD(event,6) OVER (PARTITION BY cvd_id, deployment_id ORDER BY logged_at ASC) AS next_event_6               \n"\
        "                                                                                                                  \n"\
        " FROM                                                                                                             \n"\
        "     prod.heartbeat_models.flattened                                                                              \n"\
        "                                                                                                                  \n"\
        " WHERE logged_at >= '2024-03-12'                                                                                  \n"\
        " )                                                                                                                \n"\
        "                                                                                                                  \n"\
        " // details                                                                                                       \n"\
        " SELECT                                                                                                           \n"\
        "     *                                                                                                            \n"\
        "                                                                                                                  \n"\
        " FROM                                                                                                             \n"\
        "     consecutive_hbs                                                                                              \n"\
        "                                                                                                                  \n"\
        " WHERE                                                                                                            \n"\
        "     event = 'startup'                                                                                            \n"\
        "     AND shutdown_reason = 'exception no reason found.'                                                           \n"\
        "     AND engine_events_version = '5.22.8'                                                                         \n"\
        "     AND next_cvd_voltage = next_cvd_voltage_2                                                                    \n"\
        "     AND next_cvd_voltage = next_cvd_voltage_3                                                                    \n"\
        "     AND next_cvd_voltage = next_cvd_voltage_4                                                                    \n"\
        "     AND next_cvd_voltage = next_cvd_voltage_5                                                                    \n"\
        "     AND next_cvd_voltage = next_cvd_voltage_6                                                                    \n"\
        "     AND next_cvd_voltage IS NOT NULL                                                                             \n"\
        "     AND next_cvd_voltage <> 0                                                                                    \n"\
        "     AND odometer <> 0                                                                                            \n"\
        "     AND next_event = 'periodic_update'                                                                           \n"\
        "     AND next_event_2 = 'periodic_update'                                                                         \n"\
        "     AND next_event_3 = 'periodic_update'                                                                         \n"\
        "     AND next_event_4 = 'periodic_update'                                                                         \n"\
        "     AND next_event_5 = 'periodic_update'                                                                         \n"\
        "     AND next_event_6 = 'periodic_update'                                                                         \n"\
        "                                                                                                                  \n"\
        " -- // summary                                                                                                    \n"\
        " -- SELECT                                                                                                        \n"\
        " --     customer,                                                                                                 \n"\
        " --     DATE_TRUNC('day',logged_at) logged_at_day,                                                                \n"\
        " --     engine_events_version,                                                                                    \n"\
        " --     COUNT(DISTINCT ESN) as device_count,                                                                      \n"\
        " --     COUNT(DISTINCT heartbeat_id) AS startup_hb_count                                                          \n"\
        "                                                                                                                  \n"\
        " -- FROM                                                                                                          \n"\
        " --     consecutive_hbs                                                                                           \n"\
        "                                                                                                                  \n"\
        " -- WHERE                                                                                                         \n"\
        "     -- event = 'startup'                                                                                         \n"\
        "     -- AND shutdown_reason = 'exception no reason found.'                                                        \n"\
        "     -- AND engine_events_version = '5.22.8'                                                                      \n"\
        "     -- AND next_cvd_voltage = next_cvd_voltage_2                                                                 \n"\
        "     -- AND next_cvd_voltage = next_cvd_voltage_3                                                                 \n"\
        "     -- AND next_cvd_voltage = next_cvd_voltage_4                                                                 \n"\
        "     -- AND next_cvd_voltage = next_cvd_voltage_5                                                                 \n"\
        "     -- AND next_cvd_voltage = next_cvd_voltage_6                                                                 \n"\
        "     -- AND next_cvd_voltage IS NOT NULL                                                                          \n"\
        "     -- AND next_cvd_voltage <> 0                                                                                 \n"\
        "     -- AND odometer <> 0                                                                                         \n"\
        "     -- AND next_event = 'periodic_update'                                                                        \n"\
        "     -- AND next_event_2 = 'periodic_update'                                                                      \n"\
        "     -- AND next_event_3 = 'periodic_update'                                                                      \n"\
        "     -- AND next_event_4 = 'periodic_update'                                                                      \n"\
        "     -- AND next_event_5 = 'periodic_update'                                                                      \n"\
        "     -- AND next_event_6 = 'periodic_update'                                                                      \n"\
        "                                                                                                                  \n"\
        " -- GROUP BY ALL\n"

    start_time = datetime.now()
    rsp = execute_query(connection=snowflake_conn,
                        query=sql_cmd,
                        display_response=False)

    parsed_results = get_list_of_formatted_data(df=rsp)
    save_results_to_file(response=parsed_results, filename_prefix="playground",
            sort_keyword="EVENT_TIMESTAMP_UTC")

    end_time = datetime.now()
    hours_min_seconds = str(
            timedelta(seconds=(end_time - start_time).seconds))
    sys.stdout.write("Response time: {} ( {} )\n".format(
        end_time, hours_min_seconds))



def get_data_usage():
    """
    Get data usage.
    """
    def print_table(rows):
        max_widths = []
        for column in zip(*rows):
            max_widths.append(max([len(text) for text in column]))
        template = '  '.join(['{{:<{}}}'.format(width)
                             for width in max_widths])
        return '\n'.join([template.format(*row) for row in rows])

    start_time = datetime.now()
    SEARCH_LAST_HOUR = False
    CURRENT_DATE = (datetime.now()).strftime("%Y-%m-%d").replace('-0', '-')
    global START_DATE
    START_DATE = str(START_DATE).replace('-0', '-')
    if CURRENT_DATE == START_DATE:
        SEARCH_LAST_HOUR = True

    sql_cmd = '\nSELECT * from "PROD"."DATA_USAGE"."DEVICE_DATA_USAGE_DETAILS" \n'
    if START_DATE and END_DATE and not SEARCH_LAST_HOUR:
        sql_cmd += 'WHERE created_at BETWEEN \'{}\' and \'{}\' \n'.format(
                START_DATE, END_DATE)
    else:
        sql_cmd += 'WHERE CREATED_AT BETWEEN \n'\
                   '  dateadd(day, -1, current_timestamp) \n'\
                   '  AND current_timestamp\n'
    if FILTER_BY_ENVIRONMENT != '':
        sql_cmd += 'AND utils.deployment_id_to_customer_name(deployment_id) '\
                   '= \'{}\' \n'.format(FILTER_BY_ENVIRONMENT)

    if FILTER_BY_ESN != '':
        sql_cmd += 'AND \'{}\' = CVD_ESN\n'.\
                format(FILTER_BY_ESN)

    if FILTER_BY_TABLET_SERIAL != '':
        sql_cmd += 'AND \'{}\' = TABLET_SERIAL\n'.\
                format(FILTER_BY_TABLET_SERIAL)

    sql_cmd += 'LIMIT {};\n'.format(LIMIT)
    rsp = execute_query(connection=snowflake_conn,
                        query=sql_cmd,
                        display_response=True)
    save_results_to_file(response=rsp)

    end_time = datetime.now()
    query_time = (end_time - start_time).seconds
    sys.stdout.write("\nResponse: {} took {} seconds\n".format(end_time, query_time))

    return rsp

def parse_arguments():
    """
    Parse arguments from command line, return them.
    """
    global ELEMENT, START_DATE, END_DATE, LIMIT, FILTER_BY_ENVIRONMENT, \
           FILTER_BY_UPM, FILTER_BY_JPOD_CONFIG, FILTER_BY_EE, \
           FILTER_BY_JPOD_FW, FILTER_BY_DEVICE_TYPE, FILTER_BY_LMU_CFG, \
           FILTER_BY_LMU_FW, FILTER_BY_ESN, FILTER_BY_HAPY, \
           FILE_OF_ESNS, FILTER_BY_TRACTOR_ID, FILE_OF_POWER_UNIT_IDS, \
           FILTER_BY_TRIP_ID, FILTER_BY_CITY, FILTER_BY_USER_EXTERNAL_ID, \
           FILE_OF_DRIVER_IDS, FILTER_BY_USER_ID, USE_DEV_ENV, \
           FILTER_BY_TABLET_SERIAL, FILTER_BY_VIN, USE_INT_ENV, \
           FILTER_BY_DEVICE_ID, GET_DORAN_DATA, USE_UAT_ENV, \
           FILE_OF_DEVICE_IDS, GET_PLAYGROUND_DATA
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--query_type',
                            help="monitor_release,\
                                  get_sherlock_data, \
                                  get_hos_logs, \
                                  get_driver_performance,\
                                  get_data_usage,\
                                  convert_power_ids_to_esn,\
                                  get_doran_data,\
                                  get_playground_data,\
                                  get_fluent_logs")
    arg_parser.add_argument('--limit',
            help="Limit of items in response.")
    arg_parser.add_argument('--element',
            help="Element of a query.")
    arg_parser.add_argument('--start_date',
            help="Start date: 2021-09-08")
    arg_parser.add_argument('--end_date',
            help="End date: 2021-09-15")
    arg_parser.add_argument('--filter_by_environment',
            help="{}".format(ID_TO_ENV_DICT.values()))
    arg_parser.add_argument('--filter_by_upm',
            help="UPM version, i.e, 3.22.25-286")
    arg_parser.add_argument('--filter_by_jpod_config',
            help="Jpod Config, i.e, 56")
    arg_parser.add_argument('--filter_by_jpod_fw',
            help="Jpod FW, i.e, 12q")
    arg_parser.add_argument('--filter_by_ee',
            help="EE, i.e, 5.12.0-147, 5.8.0-140")
    arg_parser.add_argument('--filter_by_device_type',
            help="LMU5530 or LMU5541")
    arg_parser.add_argument('--filter_by_lmu_cfg',
            help="I.e, 55.50")
    arg_parser.add_argument('--filter_by_trip_id',
            help="I.e, 26424")
    arg_parser.add_argument('--filter_by_lmu_fw',
            help="I.e, V4.1d")
    arg_parser.add_argument('--filter_by_esn',
            help="I.e, 5572004945")
    arg_parser.add_argument('--filter_by_tractor_id',
            help="I.e, 18026")
    arg_parser.add_argument('--filter_by_vin',
            help="I.e, 3AKJHHDR0LSLJ7510")
    arg_parser.add_argument('--filter_by_user_external_id',
            help="I.e, 17351")
    arg_parser.add_argument('--filter_by_user_id',
            help="I.e, 1584617431500269")
    arg_parser.add_argument('--filter_by_device_id',
            help="I.e, 2066658738300887")
    arg_parser.add_argument('--filter_by_city',
            help="I.e, Woodbury;")
    arg_parser.add_argument('--file_of_esns',
            help="Name of file with list of ESNs")
    arg_parser.add_argument('--file_of_driver_ids',
            help="Name of file with list of driver ids")
    arg_parser.add_argument('--file_of_power_unit_ids',
            help="Name of file with list of IDs")
    arg_parser.add_argument('--file_of_device_ids',
            help="Name of file with list of device IDs")
    arg_parser.add_argument('--filter_by_hapy',
            help="I.e, 1.1.7-16")
    arg_parser.add_argument('--use_dev_env', help="I.e, yes")
    arg_parser.add_argument('--use_int_env', help="I.e, yes")
    arg_parser.add_argument('--use_uat_env', help="I.e, yes")
    arg_parser.add_argument('--filter_by_tablet_serial',
            help="I.e, R52N40Z5C5X")

    arguments = arg_parser.parse_args()

    print("\r\nStatus of the following arguments are:\n")
    for argument in vars(arguments).items():
        print("\t{:<25} - {}".format(argument[0], argument[1]))
    print("\n")

    ELEMENT = arguments.element
    if arguments.start_date:
        START_DATE = arguments.start_date
    if arguments.end_date:
        END_DATE = arguments.end_date
    if arguments.limit:
        LIMIT = arguments.limit
    if arguments.filter_by_environment:
        if (arguments.filter_by_environment
                in ID_TO_ENV_DICT.values()):
            FILTER_BY_ENVIRONMENT = \
                    arguments.filter_by_environment
        else:
            sys.stdout.write("ERROR! Did not select valid environment!\n\
                  Please select from:\n")
            for env in ID_TO_ENV_DICT.values():
                sys.stdout.write("{}\n".format(env))
            sys.stdout.write("\n")
            sys.exit(-1)
    if arguments.filter_by_upm:
        FILTER_BY_UPM = arguments.filter_by_upm
    if arguments.filter_by_jpod_config:
        FILTER_BY_JPOD_CONFIG = arguments.filter_by_jpod_config
    if arguments.filter_by_jpod_fw:
        FILTER_BY_JPOD_FW = arguments.filter_by_jpod_fw
    if arguments.filter_by_device_type:
        FILTER_BY_DEVICE_TYPE = arguments.filter_by_device_type
    if arguments.filter_by_ee:
        FILTER_BY_EE = arguments.filter_by_ee
    if arguments.filter_by_lmu_cfg:
        FILTER_BY_LMU_CFG = arguments.filter_by_lmu_cfg
    if arguments.filter_by_lmu_fw:
        FILTER_BY_LMU_FW = arguments.filter_by_lmu_fw
    if arguments.filter_by_esn:
        FILTER_BY_ESN = arguments.filter_by_esn
    if arguments.filter_by_tractor_id:
        FILTER_BY_TRACTOR_ID = arguments.filter_by_tractor_id
    if arguments.filter_by_vin:
        FILTER_BY_VIN = arguments.filter_by_vin
    if arguments.filter_by_user_external_id:
        FILTER_BY_USER_EXTERNAL_ID = arguments.filter_by_user_external_id
    if arguments.filter_by_user_id:
        FILTER_BY_USER_ID = arguments.filter_by_user_id
    if arguments.filter_by_device_id:
        FILTER_BY_DEVICE_ID = arguments.filter_by_device_id
    if arguments.file_of_esns:
        FILE_OF_ESNS = arguments.file_of_esns
    if arguments.file_of_driver_ids:
        FILE_OF_DRIVER_IDS = arguments.file_of_driver_ids
    if arguments.file_of_power_unit_ids:
        FILE_OF_POWER_UNIT_IDS = arguments.file_of_power_unit_ids
    if arguments.file_of_device_ids:
        FILE_OF_DEVICE_IDS = arguments.file_of_device_ids
    if arguments.filter_by_hapy:
        FILTER_BY_HAPY = arguments.filter_by_hapy
    if arguments.filter_by_trip_id:
        FILTER_BY_TRIP_ID = arguments.filter_by_trip_id
    if arguments.filter_by_city:
        FILTER_BY_CITY = arguments.filter_by_city
    if arguments.use_dev_env:
        USE_DEV_ENV = arguments.use_dev_env
    if arguments.use_int_env:
        USE_INT_ENV = arguments.use_int_env
    if arguments.use_uat_env:
        USE_UAT_ENV = arguments.use_uat_env
    if arguments.filter_by_tablet_serial:
        FILTER_BY_TABLET_SERIAL = arguments.filter_by_tablet_serial

    return arguments


def setup():
    """
    Things to do to set up environment.
    """
    # NOTE: allow non distorted view on cmd line
    os.system('tput rmam')


def teardown():
    """
    Things to do to set up environment.
    """
    # NOTE: allow non distorted view on cmd line
    os.system('tput smam')


if __name__ == '__main__':
    sys.stdout.write("Welcome to Snowflake Python Queries!")
    setup()
    args = parse_arguments()

    with get_connection() as snowflake_conn:
        if args.query_type == 'monitor_release':
            monitor_release()
        elif args.query_type == 'convert_power_ids_to_esn':
            convert_power_ids_to_esn()
        elif args.query_type == 'get_sherlock_data':
            get_sherlock_data()
        elif args.query_type == 'get_hos_logs':
            get_hos_logs()
        elif args.query_type == 'get_driver_performance':
            get_driver_performance()
        elif args.query_type == 'get_data_usage':
            get_data_usage()
        elif args.query_type == 'get_doran_data':
            get_doran_data()
        elif args.query_type == 'get_playground_data':
            get_playground_data()
        elif args.query_type == 'get_fluent_logs':
            get_fluent_logs()
        else:
            sys.stdout.write("No query selected.\n")

        # TODO: Can filter by EE version, make graphs

    teardown()
    # Need to just exit, snowflake.connector.close() just hangs due to threading...
    # Seems okay via command line, PyCharm problem?
    sys.exit(0)
